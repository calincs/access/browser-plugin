FROM node:20-alpine as ce-build
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build

FROM node:20-alpine as node-runner
WORKDIR /app
COPY --from=ce-build --chown=node:node /app/apps/sidebar/dist ./dist
COPY --from=ce-build --chown=node:node /app/apps/sidebar/node_modules ./node_modules
COPY --from=ce-build --chown=node:node /app/apps/node/cidoc-images ./dist/cidoc-images
COPY --from=ce-build --chown=node:node /app/apps/node/package.json .
COPY --from=ce-build --chown=node:node /app/apps/node/dist .
COPY --from=ce-build --chown=node:node /app/node_modules ./node_modules
USER node
EXPOSE 8080
CMD ["node", "./index.js"]
