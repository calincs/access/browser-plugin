import * as DataService from "@ce-content/DataService";
import * as Constants from "@ce-content/Constants";
import { Globals, EntityNode } from "@ce-content/Globals";
import * as UIController from "@ce-content/UIController";
import L from "leaflet";

/** Extra DOM definitions for UI placements */

/**
 * Defines an outerHeight property on all elements
 */
Object.defineProperty(Element.prototype, 'outerHeight', {
  'get': function () {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const computedStyle:any = window.getComputedStyle(this);
    return parseInt(computedStyle.marginTop, 10);
  }
})

/**
* Defines an outerWidth property on all elements
*/
Object.defineProperty(Element.prototype, 'outerWidth', {
  'get': function () {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const computedStyle:any = window.getComputedStyle(this);
    return parseInt(computedStyle.marginLeft, 10);
  }
})

/** BubblesDisplayService */

/**
 * Draws the first look of the graph on screen
 * @return an empty graph center with an exit button
 */
function drawGraph() {
  if (!Globals.graphDisplayed) {
    Globals.graphDisplayed = true;
    Globals.currentTarget.style.cssText = 'color: black;';
    const target = Globals.currentTarget.getBoundingClientRect();

    // Initialize storage arrays if needed
    if (Globals.d3Data.mainContainer == null) {
      Globals.d3Data.mainContainer = [];
      Globals.d3Data.xMain = [];
      Globals.d3Data.yMain = [];
      Globals.d3Data.highlightLine = [];
      Globals.d3Data.xHighlightLine = [];
      Globals.d3Data.yHighlightLine = [];
      Globals.d3Data.xBubbles = [];
      Globals.d3Data.yBubbles = [];
      Globals.d3Data.bubblesContainer = [];
      Globals.d3Data.placementPath = [];
      Globals.d3Data.placementPathStr = [];
      Globals.d3Data.flipPlacement = [];
    }

    // Container for connecting lines (drawn on bottom of overlay stack)
    Globals.d3Data.glines = Globals.activeD3.append('g')
      .attr('id', 'lines');

    // Container for exit button
    Globals.d3Data.xExit = (target.left + window.scrollX) - ((target.left + window.scrollX) - ((target.width / 2) + (target.left + window.scrollX)));
    Globals.d3Data.yExit = (target.top + window.scrollY) - target.height - 10;
    Globals.d3Data.exitContainer = Globals.activeD3.append('g')
      .attr('transform', 'translate(' + Globals.d3Data.xExit + ', ' + Globals.d3Data.yExit + ')');

    Globals.d3Data.exitContainer.append('circle')
      .attr('r', 15)
      .attr('fill', '#107386')
      .style('cursor', 'pointer')
      .on('click', exitGraphDisplay);

    Globals.d3Data.exitContainer.append('path')
      .attr('d', "m 0 0 l 10 10 m -10 0 l 10 -10")
      .attr('transform', 'translate(-5, -5)')
      .attr('stroke', 'white')
      .attr('stroke-width', 3)
      .style('pointer-events', 'none');

    drawEntitySetup(target);
  }
}

/**
 * Draws a main bubble and preps for data bubble placement
 * @param {*} target the target to place the main bubble around
 * @return a complete setup for bubble display
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function drawEntitySetup(target: any) {
  if (Globals.d3Data.mainContainer.length > 0) {
    calcPlacement(target, false);
  } else {
    calcPlacement(target, true);
  }

  // Container for middle info
  Globals.d3Data.mainContainer.unshift(Globals.activeD3.append('g')
    .attr('transform', 'translate(' + Globals.d3Data.xMain[0] + ', ' + Globals.d3Data.yMain[0] + ')'));

  // Container for title bubble
  const titleBubbleContainer = Globals.d3Data.mainContainer[0].append('g')
    .attr('visibility', 'hidden')
    .attr('id', 'title-bubble-container');

  // Background for title bubble
  titleBubbleContainer.append('rect')
    .attr('rx', 36.5)
    .attr('width', 200)
    .attr('height', 95)
    .attr('fill', '#06596B')
    .attr('transform', 'translate(36, 40)')
    .style('filter', 'drop-shadow(0px 1px 3px rgba(0, 0, 0, 0.15)) drop-shadow(0px 1px 2px rgba(0, 0, 0, 0.30))');

  // Text for middle bubble
  titleBubbleContainer.append('text')
    .attr('id', 'title-text')
    .attr('x', 135)
    .attr('y', 90)
    .attr('dominant-baseline', 'middle')
    .attr('text-anchor', 'middle')
    .attr('fill', 'white')
    .style('font-size', '16px')
    .style('font-family', 'Inter')
    .style('line-height', 'initial')
    .style('user-select', 'none')
    .text(DataService.translateText("Loading...", Globals.locale));

  // Text for pagination
  titleBubbleContainer.append('text')
    .attr('id', 'page-text')
    .attr('x', 135)
    .attr('y', 100)
    .attr('dominant-baseline', 'middle')
    .attr('text-anchor', 'middle')
    .attr('fill', 'white')
    .style('font-size', '16px')
    .style('font-family', 'Inter')
    .style('line-height', 'initial')
    .attr('transform', 'translate(0, 15)')
    .style('user-select', 'none')
    .text("1 of 1");

  // Container for pagination components
  const paginationContainer = titleBubbleContainer.append('g')
    .attr('visibility', 'hidden')
    .attr('id', 'pagination-container');

  // Back arrow
  paginationContainer.append('circle')
    .attr('r', 12)
    .attr('fill', 'white')
    .attr('transform', 'translate(70, 113.5)')
    .style('cursor', 'pointer')
    .on('click', decreaseCurrentPage);

  // Back arrow icon
  paginationContainer.append('path')
    .attr('d', "m 10 0 l -10 7.5 l 10 7.5")
    .attr('transform', 'translate(65, 106)')
    .attr('stroke', '#083943')
    .attr('stroke-width', 3)
    .attr('fill', 'none')
    .attr('fill-opacity', '0')
    .attr('stroke-opacity', '1')
    .style('pointer-events', 'none');

  // Forward arrow
  paginationContainer.append('circle')
    .attr('r', 12)
    .attr('fill', 'white')
    .attr('transform', 'translate(200, 113.5)')
    .style('cursor', 'pointer')
    .on('click', increaseCurrentPage);

  // Forward arrow icon
  paginationContainer.append('path')
    .attr('d', "m 0 0 l 10 7.5 l -10 7.5")
    .attr('transform', 'translate(196, 106)')
    .attr('stroke', '#083943')
    .attr('stroke-width', 3)
    .attr('fill', 'none')
    .attr('fill-opacity', '0')
    .attr('stroke-opacity', '1')
    .style('pointer-events', 'none');

  // Background for middle bubble
  Globals.d3Data.mainContainer[0].append('rect')
    .attr('rx', 36.5)
    .attr('width', 270)
    .attr('height', 73)
    .attr('fill', '#083943')
    .style('filter', 'drop-shadow(0px 1px 3px rgba(0, 0, 0, 0.15)) drop-shadow(0px 1px 2px rgba(0, 0, 0, 0.30))');

  // Text for middle bubble
  Globals.d3Data.mainContainer[0].append('text')
    .attr('id', 'main-text')
    .attr('x', 135)
    .attr('y', 36.5)
    .attr('dominant-baseline', 'middle')
    .attr('text-anchor', 'middle')
    .attr('fill', 'white')
    .style('font-size', '16px')
    .style('font-family', 'Inter')
    .style('line-height', 'initial')
    .style('user-select', 'none')
    .text(Globals.currentEntity.label);

  // Create line to target
  Globals.d3Data.highlightLine.unshift(Globals.d3Data.glines.append('line')
    .attr('x1', Globals.d3Data.xHighlightLine[0])
    .attr('y1', Globals.d3Data.yHighlightLine[0])
    .attr('x2', Globals.d3Data.xMain[0] + 135)
    .attr('y2', Globals.d3Data.yMain[0] + 36.5)
    .attr('stroke-width', 5)
    .attr('stroke', '#083943')
    .attr('class', 'con-line'));

  // Container for data bubbles
  Globals.d3Data.bubblesContainer.unshift(Globals.activeD3.append('g')
    .attr('transform', 'translate(' + Globals.d3Data.xBubbles[0] + ', ' + Globals.d3Data.yBubbles[0] + ')')
    .attr('id', 'bubbles-group'));

  // Invisible placement path for data bubbles
  Globals.d3Data.placementPath.unshift(Globals.d3Data.bubblesContainer[0].append('path')
    .attr('d', Globals.d3Data.placementPathStr[0])
    .attr('style', 'pointer-events: none;')
    .attr('opacity', '0'));
}

/**
 * Draws categories for the user to choose from
 * @return Category types displaying in the bubble graph
 */
function drawCategories() {
  Globals.d3Data.mainContainer[0].select('#title-text').text(DataService.translateText("Categories", Globals.locale));
  const nodes: EntityNode[] = [];

  if (Globals.currentEntity.predicates['People'].predicates.length > 0) {
    nodes.push({
      resourceLabel: DataService.translateText("People", Globals.locale)
    });
  }
  if (Globals.currentEntity.predicates['Groups'].predicates.length > 0) {
    nodes.push({
      resourceLabel: DataService.translateText("Groups", Globals.locale)
    });
  }
  if (Globals.currentEntity.predicates['Places'].predicates.length > 0) {
    nodes.push({
      resourceLabel: DataService.translateText("Places", Globals.locale)
    });
  }
  if (Globals.currentEntity.predicates['Creative Works'].predicates.length > 0) {
    nodes.push({
      resourceLabel: DataService.translateText("Creative Works", Globals.locale)
    });
  }
  if (Globals.currentEntity.predicates['Concepts'].predicates.length > 0) {
    nodes.push({
      resourceLabel: DataService.translateText("Concepts", Globals.locale)
    });
  }
  if (Globals.currentEntity.predicates['Web References'] != null && Globals.currentEntity.predicates['Web References'].predicates.length > 0) {
    nodes.push({
      resourceLabel: DataService.translateText("Web References", Globals.locale)
    });
  }
  if (Globals.currentEntity.predicates['Other'].predicates.length > 0) {
    nodes.push({
      resourceLabel: DataService.translateText("Other", Globals.locale)
    });
  }

  if (Globals.d3Data.flipPlacement[0]) {
    nodes.reverse();
  }
  nodes.forEach((n, i) => {
    const coord = circleCoord(Globals.d3Data.placementPath[0], i, nodes.length);
    n.x = coord.x;
    n.y = coord.y;
  });

  drawCircleNodes(nodes, true);
  drawConnectingLines(nodes, []);
}

/**
 * Displays pagination in center bubble if needed
 * @return a pagination selection in the main bubble chain
 */
function setupPagination() {
  if (Globals.currentEntity.predicates[Globals.currentEntity.displayedCategory].maxPage > 1) {
    Globals.d3Data.mainContainer[0].select('#page-text').text(Globals.currentEntity.page + " of " + Globals.currentEntity.predicates[Globals.currentEntity.displayedCategory].maxPage);
    Globals.d3Data.mainContainer[0].select('#pagination-container').attr('visibility', null);
  }
}

/**
 * Draws the back button below the mainContainer
 * @return a back button for the user to return to the previous step in the chain
 */
function drawBackButton() {
  Globals.d3Data.backButton = Globals.activeD3.append('g')
    .attr('id', 'main-back-button')
    .attr('transform', 'translate(' + (Globals.d3Data.xMain[0] + 85) + ', ' + (Globals.d3Data.yMain[0] + 138) + ')');

  Globals.d3Data.backButton.append('rect')
    .attr('class', 'node')
    .attr('rx', 15)
    .attr('width', 100)
    .attr('height', 30)
    .attr('stroke-width', 2)
    .attr('stroke', '#083943')
    .attr('fill', 'white')
    .style('cursor', 'pointer')
    .style('filter', 'drop-shadow(0px 1px 3px rgba(0, 0, 0, 0.15)) drop-shadow(0px 1px 2px rgba(0, 0, 0, 0.30))')
    .on('click', () => {
      if (Globals.entityList.length == 1) {
        removeEntities();
        if (Globals.currentEntity.displayedCategory == "Web Reference Info") {
          UIController.returnToAnnotations();
          Globals.sidebar.contentWindow.postMessage({
            type: "send-category",
            category: DataService.translateText("Web References", Globals.locale)
          }, "*");
        } else {
          drawCategories();
          Globals.sidebar.contentWindow.postMessage({
            type: "send-category",
            category: "Selection"
          }, "*");
        }
      } else {
        DataService.entityTraversalBack(1);
      }
    });

  Globals.d3Data.backButton.append('text')
    .attr('x', 50)
    .attr('y', 16)
    .attr('dominant-baseline', 'middle')
    .attr('text-anchor', 'middle')
    .attr('fill', '#083943')
    .style('font-size', '16px')
    .style('font-family', 'Inter')
    .style('line-height', 'initial')
    .style('user-select', 'none')
    .style('pointer-events', 'none')
    .text("< Back");
}

/**
 * Draws data bubbles on a graph
 * @param {*} nodes the list of nodes to display
 * @return specific nodes are drawn for each entity
 */
function drawEntities(nodes: EntityNode[]) {
  if (Globals.graphDisplayed && nodes.length > 0) {
    const dataProperties = [];

    if (Globals.d3Data.flipPlacement[0]) {
      nodes.reverse();
    }
    nodes.forEach((n, i) => {
      const coord = circleCoord(Globals.d3Data.placementPath[0], i, nodes.length);
      n.x = coord.x;
      n.y = coord.y;
    });

    // Split circle and square nodes
    for (let i = 0; i < nodes.length; i++) {
      if (!nodes[i].objectIsURI) {
        dataProperties.push(nodes[i]);
        nodes.splice(i, 1);
        i--;
      }
    }

    // Display nodes
    drawSquareNodes(dataProperties);
    drawCircleNodes(nodes);

    // Display connecting lines
    drawConnectingLines(nodes, dataProperties);
  }
}

/**
 * Removes the displayed entity bubbles
 * @return all nodes are removed from the display
 */
function removeEntities() {
  if (Globals.d3Data.lines != null) {
    Globals.d3Data.lines[0].remove();
    Globals.d3Data.lines[1].remove();
  }
  
  if (Globals.d3Data.bubblesContainer != null) { 
    Globals.d3Data.bubblesContainer[0].selectAll('g.snode').remove();
    Globals.d3Data.bubblesContainer[0].selectAll('g.cnode').remove();
  }

  if (Globals.d3Data.mainContainer[0] != null) { 
    Globals.d3Data.mainContainer[0].select('#page-text').text("1 of 1");
    Globals.d3Data.mainContainer[0].select('#pagination-container').attr('visibility', 'hidden');
  }

  if (Object.prototype.hasOwnProperty.call(Globals.d3Data, "backButton") && Globals.d3Data.backButton != null) {
    Globals.d3Data.backButton.remove();
  }
}

/**
 * Exits the ui display when showing a graph on a target
 * @return Removes all nodes including the chain and resets defaults
 */
function exitGraphDisplay() {
  if (Globals.graphDisplayed) {
    Globals.currentTarget.style.cssText = 'color: black;';
    Globals.d3Data.exitContainer.remove();
    Globals.d3Data.glines.remove();

    if (Object.prototype.hasOwnProperty.call(Globals.d3Data, "backButton") && Globals.d3Data.backButton != null) {
      Globals.d3Data.backButton.remove();
    }

    // Resets
    for (let i = 0; i < Globals.d3Data.mainContainer.length; i++) {
      Globals.d3Data.mainContainer[i].remove();
      Globals.d3Data.bubblesContainer[i].remove();
    }
    Globals.entityList = [];
    Globals.d3Data.mainContainer = [];
    Globals.d3Data.xMain = [];
    Globals.d3Data.yMain = [];
    Globals.d3Data.highlightLine = [];
    Globals.d3Data.xHighlightLine = [];
    Globals.d3Data.yHighlightLine = [];
    Globals.d3Data.xBubbles = [];
    Globals.d3Data.yBubbles = [];
    Globals.d3Data.bubblesContainer = [];
    Globals.d3Data.placementPath = [];
    Globals.d3Data.placementPathStr = [];
    Globals.d3Data.flipPlacement = [];

    Globals.graphDisplayed = false;

    const locale = Globals.locale ? "en" : "fr";
    Globals.sidebar.contentWindow.location.replace(Constants.sidebarURL + locale + "/entity-select/" + Globals.numHighlights + "/100");

    Globals.sidebar.contentWindow.postMessage(
      { type: "done-highlights", highlights: Globals.highlightElements.map((element: any) => element.innerText) },
      "*"
    );

    const mouseoverEvent = new Event('mouseover');
    document.querySelectorAll(':hover').forEach(el => el.dispatchEvent(mouseoverEvent));
  }
}

/**
 * Redraws nodes to update their placement and display info
 * @return removes and re-adds nodes
 */
function redrawNodes() {
  if (Globals.d3Data.lines != null && Globals.currentEntity != null) {
    removeEntities();

    if (Globals.currentEntity.displayedCategory == 'Selection') {
      drawCategories();
    } else if (Globals.currentEntity.predicates[Globals.currentEntity.displayedCategory] != null) {
      setupPagination();

      const predicates = Globals.currentEntity.predicates[Globals.currentEntity.displayedCategory];
      const nodes = predicates.predicates.slice((Globals.currentEntity.page * 6) - 6, (Globals.currentEntity.page * 6) - 1);

      drawEntities(nodes);
      drawBackButton();
    }
  }
}

/**
 * Updates the placement of dynamic elements when the screen changes size
 * @return updates the display placement - usually in the event of a window or content resize
 */
function updatePlacement() {
  if (Globals.activeD3 != null) {
    Globals.activeD3.attr('width', 0)
      .attr('height', 0);

    Globals.activeD3.attr('width', document.documentElement.scrollWidth - 350)
      .attr('height', document.documentElement.scrollHeight)
      .attr('transform', 'translate(' + (-1 * (document.body as any).outerWidth) + ', ' + (-1 * (document.body as any).outerHeight) + ')');
  }

  if (Globals.graphDisplayed) {
    const target = Globals.currentTarget.getBoundingClientRect();

    Globals.d3Data.xExit = (target.left + window.scrollX) - ((target.left + window.scrollX) - ((target.width / 2) + (target.left + window.scrollX)));
    Globals.d3Data.yExit = (target.top + window.scrollY) - target.height - 10;
    Globals.d3Data.exitContainer.attr('transform', 'translate(' + Globals.d3Data.xExit + ', ' + Globals.d3Data.yExit + ')');

    Globals.d3Data.xMain = [];
    Globals.d3Data.yMain = [];
    Globals.d3Data.xHighlightLine = [];
    Globals.d3Data.yHighlightLine = [];
    Globals.d3Data.xBubbles = [];
    Globals.d3Data.yBubbles = [];
    Globals.d3Data.placementPathStr = [];
    Globals.d3Data.flipPlacement = [];
    for (let i = Globals.d3Data.mainContainer.length - 1; i > -1; i--) {
      if (i != Globals.d3Data.mainContainer.length - 1) {
        calcPlacement(target, false);
      } else {
        calcPlacement(target, true);
      }

      Globals.d3Data.mainContainer[i].attr('transform', 'translate(' + Globals.d3Data.xMain[0] + ', ' + Globals.d3Data.yMain[0] + ')');

      Globals.d3Data.highlightLine[i].attr('x1', Globals.d3Data.xHighlightLine[0])
        .attr('y1', Globals.d3Data.yHighlightLine[0])
        .attr('x2', Globals.d3Data.xMain[0] + 135)
        .attr('y2', Globals.d3Data.yMain[0] + 36.5);

      Globals.d3Data.bubblesContainer[i].attr('transform', 'translate(' + Globals.d3Data.xBubbles[0] + ', ' + Globals.d3Data.yBubbles[0] + ')');
      Globals.d3Data.placementPath[i].attr('d', Globals.d3Data.placementPathStr[0]);
    }

    redrawNodes();
  }
}

export { drawGraph, drawEntitySetup, drawCategories, setupPagination, drawBackButton, drawEntities, removeEntities, exitGraphDisplay, redrawNodes, updatePlacement };

/** Helper Functions */

/**
 * Calculate element placement based on target proximity to edge
 * @param {*} target the highlighted element
 * @param {*} firstBubble a boolean indicating the drawing method
 * @return x/y coords for where to place bubble components
 */
function calcPlacement(target: { left: number; width: number; top: number; height: number; }, firstBubble: boolean) {
  const scale = 72;
  if (firstBubble) { // If this is the first bubble drawn
    Globals.d3Data.xMain.unshift(((window.innerWidth - 350) / 2) - 135);
    Globals.d3Data.yMain.unshift((target.top + window.scrollY) + target.height + 225);

    Globals.d3Data.xHighlightLine.unshift((target.left + window.scrollX) + (target.width / 2));
    Globals.d3Data.yHighlightLine.unshift((target.top + window.scrollY) + target.height);

    Globals.d3Data.placementPathStr.unshift(`m ${2 * scale} 0 c ${-8 * scale} ${7 * scale} ${13.5 * scale} ${7 * scale} ${5.5 * scale} 0`);
    Globals.d3Data.flipPlacement.unshift(true);

    if ((Globals.d3Data.yMain[0] + 36.5 + 294) > document.documentElement.scrollHeight) { // Handle out of bounds on bottom
      Globals.d3Data.yMain[0] = Globals.d3Data.yMain[0] - 800;
    }
  } else { // If this is a bubble traversal drawn
    Globals.d3Data.xMain.unshift(Globals.d3Data.xMain[0]);
    Globals.d3Data.yMain.unshift(Globals.d3Data.yMain[0] + 78);
    Globals.d3Data.xHighlightLine.unshift(Globals.d3Data.xMain[0] + 135);
    Globals.d3Data.yHighlightLine.unshift(Globals.d3Data.yMain[0] - 10);
    Globals.d3Data.placementPathStr.unshift(`m ${2 * scale} 0 c ${-8 * scale} ${7 * scale} ${13.5 * scale} ${7 * scale} ${5.5 * scale} 0`);
    Globals.d3Data.flipPlacement.unshift(true);
  }

  Globals.d3Data.xBubbles.unshift((Globals.d3Data.xMain[0] - ((351.115 + Globals.d3Data.xMain[0]) - (135 + Globals.d3Data.xMain[0]))) + 9);
  Globals.d3Data.yBubbles.unshift((Globals.d3Data.yMain[0] - ((194.37 + Globals.d3Data.yMain[0]) - (36.5 + Globals.d3Data.yMain[0]))) + 54);
}

/**
 * Draws square nodes for drawEntities
 * @param {*} nodes the nodes to draw
 * @return square nodes are processed for display
 */
function drawSquareNodes(nodes: EntityNode[]) {
  const snodes = Globals.d3Data.bubblesContainer[0].selectAll('g.snode')
    .data(nodes).enter().append('g')
    .attr('transform', (d: { x: number; y: number; }) => 'translate(' + (d.x - 100) + ',' + (d.y - 90) + ')')
    .classed('snode', true);

  snodes.append('rect') // Create data bubbles
    .attr('width', 200)
    .attr('height', 200)
    .attr('stroke-width', 1)
    .attr('stroke', '#083943')
    .attr('fill', 'white')
    .attr('class', 'snode-shape')
    .style('filter', 'drop-shadow(0px 1px 3px rgba(0, 0, 0, 0.15)) drop-shadow(0px 1px 2px rgba(0, 0, 0, 0.30))');

  const htmlContainer = snodes.append('foreignObject') // Create data text
    .attr('x', 0)
    .attr('y', 0)
    .attr('width', 200)
    .attr('height', 200)
    .style('pointer-events', 'none')

  htmlContainer.append('xhtml:p')
    .style('color', 'black')
    .style('background-color', '#EDF6F8')
    .style('border-bottom', '1px #083943 solid')
    .style('font-size', '15px')
    .style('font-family', 'Inter')
    .style('line-height', 'initial')
    .style('user-select', 'none')
    .style('pointer-events', 'none')
    .style('margin', '1px 0px 0px 1px')
    .style('padding-top', '7px')
    .style('width', '198px')
    .style('height', '24px')
    .style('text-align', 'center')
    .style('overflow-wrap', 'break-word')
    .style('overflow', 'hidden')
    .style('box-sizing', 'content-box')
    .style('display', '-webkit-box')
    .style('-webkit-line-clamp', 8)
    .style('-webkit-box-orient', 'vertical')
    .style('-webkit-box-pack', 'center')
    .html((d: { predicate: string; predicateLabel: null; }) => {
      let label = d.predicate;
      if (d.predicateLabel != null) {
        label = d.predicateLabel;
      }
      return label;
    });

  const contentContainer = htmlContainer.append('xhtml:div')
    .style('color', 'black')
    .style('font-size', '15px')
    .style('font-family', 'Inter')
    .style('line-height', 'initial')
    .style('user-select', 'none')
    .style('margin', 0)
    .style('padding', '7.5px 10px 7.5px 10px')
    .style('width', 'calc(100% - 20px)')
    .style('height', 'calc(100% - 48px)')
    .style('text-align', 'center')
    .style('overflow-wrap', 'break-word')
    .style('overflow-y', 'auto')
    .style('box-sizing', 'content-box');

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const mapsDrawn: any[] = [];

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  contentContainer.html(function (d: { object: any; objectLabel: null; type: string; image: string; birthDate: null; birthPlace: string; deathDate: null; deathPlace: string; description: string; coords: null; }) {
    let label = d.object;
    if (d.objectLabel != null) {
      label = d.objectLabel;
    }

    let htmlContent = label;
    if (d.type == "http://www.cidoc-crm.org/cidoc-crm/E21_Person") {
      htmlContent = "<b>" + label + "</b><br/>";

      if (d.image) {
        const element = this as HTMLElement;
        element.style.backgroundImage = 'url("' + d.image + '")';
        element.style.backgroundSize = 'cover';
        element.style.boxShadow = 'inset 0 0 0 100vmax rgba(0, 0, 0, .5)';
        element.style.color = 'white';
      }
      if (d.birthDate || d.birthPlace) {
        htmlContent += "<br/>Born: ";
        if (d.birthDate) {
          htmlContent += d.birthDate;
        }
        if (d.birthPlace) {
          htmlContent += d.birthDate != null ? ", " + d.birthPlace : d.birthPlace;
        }
      }
      if (d.deathDate || d.deathPlace) {
        htmlContent += "<br/>Died: ";
        if (d.deathDate) {
          htmlContent += d.deathDate;
        }
        if (d.deathPlace) {
          htmlContent += d.deathDate != null ? ", " + d.deathPlace : d.deathPlace;
        }
      }
      if (d.description) {
        htmlContent += "<br/>" + d.description;
      }
    } else if (d.type == "http://www.cidoc-crm.org/cidoc-crm/E53_Place") {
      if (d.coords == null) {
        htmlContent = "<b>" + label + "</b>";
      } else {
        htmlContent = "<b>" + label + "</b><br/><div id='map-" + mapsDrawn.length + "' style='height: 135px; width: 180px;'></div>";
        mapsDrawn.push(d.coords);
      }
    }

    return htmlContent;
  });

  for (const [i, coords] of mapsDrawn.entries()) {
    const leaflet = L.map('map-' + i, {
      dragging: false,
      zoomControl: false,
      scrollWheelZoom: false,
      doubleClickZoom: false,
      boxZoom: false,
      attributionControl: false,
      keyboard: false
    }).setView(coords, 8);

    L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
      attribution: 'Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012',
    }).addTo(leaflet);
  }
}

/**
 * Draws circle nodes for drawEntities
 * @param {*} nodes the nodes to draw
 * @return circle nodes are processed for display
 */
function drawCircleNodes(nodes: EntityNode[], areCatgories = false) {
  const cnodes = Globals.d3Data.bubblesContainer[0].selectAll('g.cnode')
    .data(nodes).enter().append('g')
    .attr('transform', (d: { x: string; y: string; }) => 'translate(' + d.x + ',' + d.y + ')')
    .classed('cnode', true);

  cnodes.append('circle') // Create data bubbles
    .attr('r', 75)
    .attr('stroke-width', 5)
    .attr('stroke', '#083943')
    .attr('fill', 'white')
    .attr('class', 'cnode-shape')
    .attr('cursor', 'pointer')
    .style('filter', 'drop-shadow(0px 1px 3px rgba(0, 0, 0, 0.15)) drop-shadow(0px 1px 2px rgba(0, 0, 0, 0.30))')
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    .on('click', (d: { target: { __data__: { typeLabel: string; resourceLabel: any; resource: any; }; }; }) => {
      if (!areCatgories && d.target.__data__.typeLabel != "Annotation") {
        DataService.addNewFocus({
          type: "show-entity-graph",
          label: d.target.__data__.resourceLabel ?? d.target.__data__.resource,
          uri: d.target.__data__.resource,
          page: 1,
          predicates: {},
          displayedCategory : "",
        });
      } else if (!areCatgories) {
        Globals.sidebar.contentWindow.postMessage({
          type: "send-annotation",
          annotation: d.target.__data__
        }, "*");
      } else {
        Globals.sidebar.contentWindow.postMessage({
          type: "send-category",
          category: DataService.translateText(d.target.__data__.resourceLabel, Globals.locale)
        }, "*");

        DataService.drawCategoryEntities(DataService.translateText(d.target.__data__.resourceLabel, true));
      }
    });

  const foreign = cnodes.append('foreignObject') // Create data text
    .attr('x', -60)
    .attr('y', -35)
    .attr('width', 120)
    .attr('height', 70)
    .style('pointer-events', 'none');

  const div = foreign.append('xhtml:div')
    .style('width', '120px')
    .style('height', '70px')
    .style('pointer-events', 'none');

  const p = div.append('xhtml:p')
    .style('color', 'black')
    .style('font-size', '15px')
    .style('font-family', 'Inter')
    .style('line-height', 'initial')
    .style('user-select', 'none')
    .style('pointer-events', 'none')
    .style('margin', 0)
    .style('width', '120px')
    .style('text-align', 'center')
    .style('overflow-wrap', 'break-word')
    .style('overflow', 'hidden')
    .style('box-sizing', 'content-box')
    .style('display', '-webkit-box')
    .style('-webkit-line-clamp', 4)
    .style('-webkit-box-orient', 'vertical')
    .style('-webkit-box-pack', 'center')
    .html((d: { resource: string; resourceLabel: null; }) => {
      let label = d.resource;
      if (d.resourceLabel != null) {
        label = d.resourceLabel;
      }
      return label;
    });

  if (areCatgories) {
    div.style('display', 'flex')
      .style('align-items', 'center')
      .style('transform', 'translate(0px, -4px)');

    p.style('font-size', '16px')
      .style('font-weight', 'bold')
      .style('color', 'rgb(8, 57, 67)')
      .style('max-height', '70px');
  } else {
    p.style('height', '70px');
  }
}

/**
 * Draw the lines that connect the info bubles to the main bubble
 * @param {*} nodes 
 * @param {*} dataProperties 
 * @return lines connection nodes are processed for display
 */
function drawConnectingLines(nodes: EntityNode[], dataProperties: EntityNode[]) {
  Globals.d3Data.lines = [];

  Globals.d3Data.lines[0] = Globals.d3Data.glines.selectAll('con-line')
    .data(nodes).enter().append('line')
    .attr('x1', Globals.d3Data.xMain[0] + 135)
    .attr('y1', Globals.d3Data.yMain[0] + 36.5)
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    .attr('x2', (d: { x: any; }) => d.x + Globals.d3Data.xBubbles[0])
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    .attr('y2', (d: { y: any; }) => d.y + Globals.d3Data.yBubbles[0])
    .attr('stroke-width', 5)
    .attr('stroke', '#083943')
    .attr('class', 'con-line');

  Globals.d3Data.lines[1] = Globals.d3Data.glines.selectAll('con-line')
    .data(dataProperties).enter().append('line')
    .attr('x1', Globals.d3Data.xMain[0] + 135)
    .attr('y1', Globals.d3Data.yMain[0] + 36.5)
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    .attr('x2', (d: { x: any; }) => d.x + Globals.d3Data.xBubbles[0])
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    .attr('y2', (d: { y: any; }) => d.y + Globals.d3Data.yBubbles[0])
    .attr('stroke-width', 5)
    .attr('stroke', '#083943')
    .attr('class', 'con-line');
}

/**
 * Increases the page displayed on the graph
 * @return the nodes displayed show the next page
 */
function increaseCurrentPage() {
  if (Globals.currentEntity.page != Globals.currentEntity.predicates[Globals.currentEntity.displayedCategory].maxPage) {
    Globals.currentEntity.page += 1;
  }
  Globals.d3Data.bubblesContainer[0].selectAll('g.snode').remove();
  Globals.d3Data.bubblesContainer[0].selectAll('g.cnode').remove();
  Globals.d3Data.lines[0].remove();
  Globals.d3Data.lines[1].remove();

  const predicates = Globals.currentEntity.predicates[Globals.currentEntity.displayedCategory];
  const nodes = predicates.predicates.slice((Globals.currentEntity.page * 5) - 5, (Globals.currentEntity.page * 5));

  drawEntities(nodes);
  Globals.d3Data.mainContainer[0].select('#page-text').text(Globals.currentEntity.page + " of " + Globals.currentEntity.predicates[Globals.currentEntity.displayedCategory].maxPage);
}

/**
 * Decreases the page displayed on the graph
 * @return the nodes displays show the previous page
 */
function decreaseCurrentPage() {
  if (Globals.currentEntity.page != 1) {
    Globals.currentEntity.page -= 1;
  }
  Globals.d3Data.bubblesContainer[0].selectAll('g.snode').remove();
  Globals.d3Data.bubblesContainer[0].selectAll('g.cnode').remove();
  Globals.d3Data.lines[0].remove();
  Globals.d3Data.lines[1].remove();

  const predicates = Globals.currentEntity.predicates[Globals.currentEntity.displayedCategory];
  const nodes = predicates.predicates.slice((Globals.currentEntity.page * 5) - 5, (Globals.currentEntity.page * 5));

  drawEntities(nodes);
  Globals.d3Data.mainContainer[0].select('#page-text').text(Globals.currentEntity.page + " of " + Globals.currentEntity.predicates[Globals.currentEntity.displayedCategory].maxPage);
}

/**
 * Evenly spaces nodes along arc
 * @param {*} node 
 * @param {*} index 
 * @param {*} num_nodes 
 * @returns an x/y coord for where to place the node in the graph
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function circleCoord(placementCircle: { node: () => { (): any; new(): any; getTotalLength: { (): any; new(): any; }; getPointAtLength: { (arg0: any): any; new(): any; }; }; }, index: number, num_nodes: number) {
  const circumference = placementCircle.node().getTotalLength();
  const pointAtLength = function (l: number) { return placementCircle.node().getPointAtLength(l) };
  const sectionLength = (circumference) / num_nodes;
  const position = sectionLength * index + sectionLength / 2;
  return pointAtLength(circumference - position)
}
