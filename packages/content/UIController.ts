/* eslint-disable @typescript-eslint/no-explicit-any */
import * as DataService from "@ce-content/DataService";
import * as BubblesDisplayService from "@ce-content/BubblesDisplayService";
import * as Constants from "@ce-content/Constants";
import { Entities, Entity, Globals } from "@ce-content/Globals";
declare const d3: any;

// The message controller is swapped depending on the build
let MessageController: any = null;
import(
  process.env.BUILD_ENV == 'PLUGIN' ? 
  "@/packages/content/PluginMessageController" : 
  "@/packages/content/LibraryMessageController"
).then((controller) => {MessageController = controller});

/**
 * Sets up the UI when the extension is first launched
 * @return DOM is set up with required UI component containers
 */
function setupUIOverlays() {
  // Imports for fonts
  let link = document.createElement('link');
  link.type = "text/css";
  link.rel = "stylesheet";
  document.head.appendChild(link);
  link.href = "https://fonts.googleapis.com/css2?family=Inter:wght@100..900&display=swap";

  // Setup the body for extension exit
  Globals.bodyCopy = document.body.innerHTML;
  document.body.style.width = "calc(100vw - 350px)";
  document.body.style.position = "relative";
  document.body.style.overflowX = "auto";

  // Setup the sidebar iframe
  Globals.sidebarSelections = document.createElement('div');
  Globals.sidebarSelections.style.cssText = Constants.sidebarSelectionsCss;
  Globals.sidebarSelections.innerHTML = Constants.sidebarSelectionsHtml;
  Globals.sidebarSelections.setAttribute("role", "complementary");
  document.body.append(Globals.sidebarSelections);
  document.getElementById('lincs-toggle-sidebar')?.addEventListener('click', toggleSidebar);
  document.getElementById('lincs-toggle-sidebar')?.addEventListener("keypress", (event) => {
    if (event.key === "Enter") {
      event.preventDefault();
      toggleSidebar();
    }
  });
  document.getElementById('lincs-toggle-vis')?.addEventListener('click', toggleDisplayGraph);
  document.getElementById('lincs-toggle-vis')?.addEventListener("keypress", (event) => {
    if (event.key === "Enter") {
      event.preventDefault();
      toggleDisplayGraph();
    }
  });

  Globals.sidebar = document.createElement('iframe');
  Globals.sidebar.allow = "clipboard-write";
  Globals.sidebar.style.cssText = Constants.sidebarCss;
  Globals.sidebar.setAttribute("title", "LINCS Context Explorer Sidebar");
  Globals.sidebar.setAttribute("role", "complementary");

  MessageController.getSettings().then((response: any) => {
    Globals.locale = response.activeSettings.hidden.settings.languageSelection.value;
    const locale = response.activeSettings.hidden.settings.languageSelection.value ? "en" : "fr";
    Globals.sidebar.src = Constants.sidebarURL + locale + "/entity-select/" + Globals.numHighlights + "/0";
    document.body.append(Globals.sidebar);

    // Give time for sidebar to render in DOM
    setTimeout(() => {
      Globals.sidebar.contentWindow.focus();
    }, 500);
  });

  // Setup D3 for bubbles
  Globals.activeD3 = d3.select('body')
    .append('svg')
    .attr('id', 'lincs-svg-display')
    .attr('width', document.documentElement.scrollWidth - 350)
    .attr('height', document.documentElement.scrollHeight)
    .attr('transform', 'translate(' + (-1 * (document.body as any).outerWidth) + ', ' + (-1 * (document.body as any).outerHeight) + ')');

  // Add event listeners for resize and close
  window.addEventListener("resize", debounce(BubblesDisplayService.updatePlacement));
  window.addEventListener("keydown", (event) => {
    if (event.key == "Escape") {
      BubblesDisplayService.exitGraphDisplay();
    }
  });
  document.onclick = (event) => {
    if (document.getElementById("lincs-svg-display") != null) {
      const targetElement = event.target as HTMLElement;
      if (targetElement && !Globals.activeD3._groups[0][0].contains(targetElement) && !(targetElement.getAttribute("class") && targetElement.getAttribute("class")?.includes("node"))) {
        BubblesDisplayService.exitGraphDisplay();
      }
    }
  }
}

/**
 * Exits the extension and cleans up DOM
 * @return cleaned up DOM
 */
function exitExtension() {
  document.body.style.width = "inherit";
  document.body.style.position = "inherit";
  document.body.innerHTML = Globals.bodyCopy;

  // Reset Globals
  Globals.numHighlights = 0;
  Globals.inFlightRequests = [];
  Globals.sidebar = null;
  Globals.activeD3 = null;
  Globals.d3Data = {};
  Globals.graphDisplayed = false;
  Globals.currentTarget = null;
  Globals.currentEntity = {
    uri: "",
    label: "",
    page: 1,
    predicates: {},
    displayedCategory: "",
  };
  Globals.entityList = [];
  Globals.highlightedWords = [];
  Globals.allNodes = [];
}

/**
 * Displayed as soon as a user hovers over a highlight for the first time
 * @param {*} event the hover event
 * @return the graph setup and the sidebar showing entity candidates
 */
function getMatchedEntities(event: any, skipDebounceCheck = false, uri = null) {
  if (!Globals.graphDisplayed && !Globals.loading) {
    setTimeout(() => {
      let debounceCheck = false;
      document.querySelectorAll(':hover').forEach((el) => {
        if (el == event.target) {
          debounceCheck = true;
        }
      });

      if (debounceCheck || skipDebounceCheck) {
        // To open the sidebar if it is collapsed on highlight hover
        if (Globals.sidebar.style.display == "none") {
          toggleSidebar();
        }

        Globals.currentTarget = event.target;
        Globals.currentEntity = {
          uri: "http://id.lincsproject.ca",
          label: DataService.translateText("Select a matching entity", Globals.locale) + " →",
          page: 1,
          predicates: {},
          displayedCategory: ""
        }

        BubblesDisplayService.drawGraph();
        if (uri == null) {
          const locale = Globals.locale ? "en" : "fr";
          Globals.sidebar.contentWindow.location.replace(Constants.sidebarURL + locale + "/entity-select/" + Globals.numHighlights + "/100/" + encodeURIComponent(Globals.currentTarget.textContent));
        } else {
          const entity: Entity = { label: "Confirmed Entity", uri: uri, page: 1, predicates: {}, displayedCategory: "" };
          DataService.showEntityGraph(entity, true);
        }
      }
    }, 250);
  }
}

/**
 * Re-displays the select entity list when a user presses a back button
 * @return Sets the graph display back to awaiting a candidate entity selection
 */
function showEntitySelect() {
  Globals.currentEntity = {
    uri: "http://id.lincsproject.ca",
    label: DataService.translateText("Select a matching entity", Globals.locale) + " →",
    page: 1,
    predicates: {},
    displayedCategory: ""
  }
  DataService.popNumEntities(Globals.d3Data.mainContainer.length - 1);
  Globals.d3Data.mainContainer[0].select('#main-text').text(Globals.currentEntity.label);
  Globals.d3Data.mainContainer[0].select('#title-bubble-container').attr('visibility', 'hidden');

  const locale = Globals.locale ? "en" : "fr";
  Globals.sidebar.contentWindow.location.replace(Constants.sidebarURL + locale + "/entity-select/" + Globals.numHighlights + "/100/" + encodeURIComponent(Globals.currentTarget.textContent));

  Globals.entityList = [];
  BubblesDisplayService.removeEntities();
}

/**
 * Auto scrolls to the given highlight
 * @return the view port focussing on a specific highlight
 */
function scrollToHighlight(highlightNum: number, selectHighlightImmediatly: boolean) {
  if (Globals.focussedHighlight) {
    Globals.focussedHighlight.style.backgroundColor = '#C2E5FF';
  }
  Globals.focussedHighlight = Globals.highlightElements[highlightNum];
  if (Globals.focussedHighlight) {
    Globals.focussedHighlight.style.backgroundColor = '#008efa';
    Globals.focussedHighlight.scrollIntoView({
      behavior: 'auto',
      block: 'center',
      inline: 'center'
    });
  }

  if (selectHighlightImmediatly) {
    getMatchedEntities({ target: Globals.highlightElements[highlightNum] }, true);
  }
}

/**
 * Sets the bubble display back to the annotations category
 */
function returnToAnnotations() {
  DataService.popNumEntities(1, false);
  Globals.d3Data.mainContainer[0].select('#title-bubble-container').attr('visibility', null);
  DataService.drawCategoryEntities("Web References");
}

/**
 * Displays the info attached to a single annotation in the bubble display
 * @param {*} annotation the annotation to display
 */
function displayAnnotationInfo(annotation: Entities) {
  let label = annotation.mainEntityInfo.resourceLabel ?? annotation.mainEntityInfo.resource;
  if (label.length > 27) {
    label = label.substring(0, 26) + "...";
  }

  BubblesDisplayService.removeEntities();
  Globals.d3Data.mainContainer[0].select('#title-bubble-container').attr('visibility', 'hidden');

  Globals.currentEntity = {
    uri: annotation.mainEntityInfo.resource,
    label: label,
    page: 1,
    displayedCategory: "Web Reference Info",
    predicates: Globals.currentEntity.predicates,
  }
  annotation.mainEntityInfo.outgoing.forEach((predicate) => {
    predicate.objectIsURI = false;
  });
  Globals.currentEntity.predicates["Web Reference Info"] = {
    maxPage: Math.ceil(annotation.mainEntityInfo.outgoing.length / 5),
    predicates: annotation.mainEntityInfo.outgoing,
  };

  const target = Globals.currentTarget.getBoundingClientRect();
  BubblesDisplayService.drawEntitySetup(target);
  Globals.d3Data.mainContainer[0].select('#title-bubble-container').attr('visibility', null);
  Globals.d3Data.mainContainer[0].select('#title-text').text(DataService.translateText("Connections", Globals.locale));

  BubblesDisplayService.setupPagination();
  BubblesDisplayService.drawBackButton();
  BubblesDisplayService.drawEntities(Globals.currentEntity.predicates[Globals.currentEntity.displayedCategory].predicates.slice(0, 5));
}

function expandSidebarWidth(currentWidth = parseInt(Globals.sidebar.style.width.match(/\d+/))) {
  if (currentWidth < 550) {
    currentWidth += 5;
    Globals.sidebar.style.width = currentWidth.toString() + 'px';

    if (Globals.sidebar.style.display != "none") {
      Globals.sidebarSelections.style.right = currentWidth.toString() + 'px';
      document.body.style.width = "calc(-" + currentWidth.toString() + "px + 100vw)";
    }

    setTimeout(() => expandSidebarWidth(currentWidth), 4);
  } else {
    BubblesDisplayService.updatePlacement();
  }
}

function contractSidebarWidth(currentWidth = parseInt(Globals.sidebar.style.width.match(/\d+/))) {
  if (currentWidth > 350) {
    currentWidth -= 5;
    Globals.sidebar.style.width = currentWidth.toString() + 'px';

    if (Globals.sidebar.style.display != "none") {
      Globals.sidebarSelections.style.right = currentWidth.toString() + 'px';
      document.body.style.width = "calc(-" + currentWidth.toString() + "px + 100vw)";
    }
    
    setTimeout(() => contractSidebarWidth(currentWidth), 2);
  } else {
    BubblesDisplayService.updatePlacement();
  }
}

export { setupUIOverlays, exitExtension, getMatchedEntities, showEntitySelect, scrollToHighlight, returnToAnnotations, displayAnnotationInfo, expandSidebarWidth, contractSidebarWidth };

/** Helper Functions */

/**
 * Toggle the bubble display on and off
 * @return the bubble display either displaying or hidden
 */
function toggleDisplayGraph() {
  const svgDisplay = document.getElementById("lincs-svg-display");
  const graphShown = document.getElementById("lincs-graph-shown");
  const graphHidden = document.getElementById("lincs-graph-hidden");
  if (svgDisplay && graphShown && graphHidden) {
    if (svgDisplay.style.display == "none") {
      svgDisplay.style.display = "unset";
      graphShown.style.display = "unset";
      graphHidden.style.display = "none";
    } else {
      svgDisplay.style.display = "none";
      graphShown.style.display = "none";
      graphHidden.style.display = "unset";
    }
  }
}

/**
 * Toggle the sidebar display on and off
 * @return the sidbar display either displaying or hidden
 */
function toggleSidebar() {
  let sidebarWidth = parseInt(Globals.sidebar.style.width.match(/\d+/))
  if (!(sidebarWidth == 350 || sidebarWidth == 550)) {
    return;
  }

  const lincsMax = document.getElementById("lincs-maximize");
  const lincsMin = document.getElementById("lincs-minimize");
  if (Globals.sidebar.style.display == "none") {
    Globals.sidebar.style.display = "unset";
    document.body.style.width = "calc(100vw - 350px)";
    Globals.sidebarSelections.style.right = sidebarWidth + "px";
    if (lincsMax) lincsMax.style.display = "none";
    if (lincsMin) lincsMin.style.display = "unset";
  } else {
    Globals.sidebar.style.display = "none";
    document.body.style.width = "100vw";
    Globals.sidebarSelections.style.right = "0px"
    if (lincsMax) lincsMax.style.display = "unset";
    if (lincsMin) lincsMin.style.display = "none";
  }
  BubblesDisplayService.updatePlacement();
}

/**
 * Debounce an event listener to wait for the end of the action
 * @param {*} func the function to run at the end of the action
 * @returns a timeout function
 */
function debounce(func: any) {
  let timer: any;
  return function (event: any) {
    if (timer) clearTimeout(timer);
    timer = setTimeout(func, 100, event);
  };
}
