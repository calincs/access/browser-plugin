// Create background service worker
let serviceWorker: ServiceWorker|null = null;
navigator.serviceWorker.register((window as any).lincs_params.service_worker);
navigator.serviceWorker.ready.then((registration) => {
  serviceWorker = registration.active;
});

// Create BroadcastChannel for responses from the background service worker
const responseChannel = "lincs-message-" + makeId(5);
const channel = new BroadcastChannel(responseChannel);

/**
 * Get the settings object
 * @returns a promise for the returned settings
 */
function getSettings() {
  return new Promise((resolve) => {
    resolve({activeSettings: (window as any).lincs_params.settings});
  });
}

/**
 * Get the filters object
 * @returns a promise for the returned filters
 */
function getFilters() {
  return new Promise((resolve) => {
    resolve({activeFilters: (window as any).lincs_params.filters});
  });
}

/**
 * Send a message and wait for response
 * @param message The message being sent
 * @returns A promise for the returned data
 */
function sendMessage(message: any) {
  return new Promise((resolve) => {
    message.responseChannel = responseChannel;
    message.settings = (window as any).lincs_params.settings;
    message.filters = (window as any).lincs_params.filters;
    serviceWorker?.postMessage(message);

    channel.addEventListener("message", (response: any) => {
      if (response.data.type == message.type) {
        response.srcElement.removeEventListener("message", arguments.callee);
        resolve(response.data);
      }
    });
  });
}

export { getSettings, getFilters, sendMessage };

/**
 * Creates a random string of characters
 * @param length the number of random characters to return
 * @returns a random string of characters of size length
 */
function makeId(length: number) {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  let counter = 0;
  while (counter < length) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
    counter += 1;
  }
  return result;
}
