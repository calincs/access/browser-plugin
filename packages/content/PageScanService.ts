/* eslint-disable @typescript-eslint/no-explicit-any */
import * as UIController from "@ce-content/UIController";
import * as Constants from "@ce-content/Constants";
import { Globals } from "@ce-content/Globals";

// The message controller is swapped depending on the build
let MessageController: any = null;
import(
  process.env.BUILD_ENV == 'PLUGIN' ? 
  "@/packages/content/PluginMessageController" : 
  "@/packages/content/LibraryMessageController"
).then((controller) => {MessageController = controller});

/**
 * Grabs the text from the site, sends it to the background script to scan, then highlights results on page
 * @param {*} reply the reply callback for the requesting message
 * @return Highlights found matches on the DOM
 */
function fullScanAndHighlight(reply: any) {
  reply({ type: "success" });

  if (document.getElementById("lincs-svg-display") !== null) {
    UIController.exitExtension();
  }
  UIController.setupUIOverlays();
  Globals.totalRequests = 1;
  Globals.finishedRequests = 0;
  Globals.inFlightRequests = [];
  Globals.loading = true;

  let id = createScanId(10);
  Globals.scanId = id;

  scanInlineURIs(document.body).then(() => {
    scanTextNodes(document.body, id);
  });
}

/**
 * Grabs the highlighted text, sends it to the service worker to scan, then highlights results on page
 * @param {*} reply the reply callback for the requesting message
 * @return Highlights found matches on the DOM
 */
function textScanAndHighlight(reply: any) {
  const wSel = window.getSelection();
  if (wSel && wSel.anchorNode != null && wSel.anchorNode?.nodeType == 3) {
    reply({ type: "success" });
    const selection = wSel.getRangeAt(0);
    const selectedText = selection.extractContents();
    const span = document.createElement("span");
    span.appendChild(selectedText);
    selection.insertNode(span);

    UIController.setupUIOverlays();
    Globals.totalRequests = 1;
    Globals.finishedRequests = 0;
    Globals.inFlightRequests = [];
    Globals.loading = true;

    let id = createScanId(10);
    Globals.scanId = id;

    scanInlineURIs(span).then(() => {
      scanTextNodes(span, id);
    });
  }
}

/**
 * Stops an in progress scan
 * @return exits the extension or cancelles all further scanning
 */
function stopScan() {
  Globals.loading = false;
  Globals.scanId = "";
  if (Globals.numHighlights == 0) {
    UIController.exitExtension();
  } else {
    const locale = Globals.locale ? "en" : "fr";
    Globals.sidebar.contentWindow.location.replace(Constants.sidebarURL + locale + "/entity-select/" + Globals.numHighlights + "/100");

    Globals.highlightElements = Array.from(document.querySelectorAll(".lincs-mark"));
    Globals.highlightElements = Globals.highlightElements.filter(element => {
      return element.innerText != "";
    });
    Globals.sidebar.contentWindow.postMessage(
      { type: "done-highlights", highlights: Globals.highlightElements.map(element => element.innerText) },
      "*"
    );
  }
}

/**
 * Returns a boolean that determines if the scan highlighted text button should be enabled
 * @param {*} reply the reply callback for the requesting message
 * @return a boolean that indicates whether a highlighted scan can be started
 */
function checkHighlightScan(reply: any) {
  const wSel = window.getSelection();
  if (!wSel || wSel.anchorNode == null || wSel.anchorNode.nodeType != 3 || document.getElementById("lincs-svg-display") !== null) {
    reply({ enable: false });
  } else {
    reply({ enable: true });
  }
}

/**
 * Posts a message to the sidebar containing the active filters
 * @return a message sent to the sidebar iframe containing a list of active filters
 */
function getFiltersToIframe() {
  MessageController.getFilters().then((response: any) => {
    Globals.sidebar.contentWindow.postMessage(
      { type: "return-filters", filters: response },
      "*"
    );
  });
}

/**
 * Posts a message to the sidebar containing the active settings
 * @return a message sent to the sidebar iframe containing a list of active settings
 */
function getSettingsToIframe() {
  MessageController.getSettings().then((response: any) => {
    Globals.sidebar.contentWindow.postMessage(
      { type: "return-settings", settings: response },
      "*"
    );
  });
}


export { fullScanAndHighlight, textScanAndHighlight, stopScan, checkHighlightScan, getFiltersToIframe, getSettingsToIframe };

/** Helper Functions */

function createScanId(length: number) {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  let counter = 0;
  while (counter < length) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
    counter += 1;
  }
  return result;
}

function scanInlineURIs(scanElement: HTMLElement) {
  return new Promise<void>((resolve) => {
    const sTags = Array.from(scanElement.getElementsByTagName("a"));
    const aTags = sTags.filter((tag: any) => {
      return (tag.getAttribute("href") != null);
    });

    const aTagsMap:any = {};
    for (const tag of aTags) {
      const href: string = tag.getAttribute("href") ?? "";
      if (!aTagsMap[href]) aTagsMap[href] = [];
      aTagsMap[href].push(tag);
    }

    MessageController.sendMessage({ type: 'scan-URIs', uris: aTags.map((tag) => tag.getAttribute("href")) }).then(async (response: any) => {
      if (response && response[0] != "LINCS-ERROR") {
        for (const matchedURI of response) {
          const elementsToHighlight = aTagsMap[matchedURI];

          for (const elementToHighlight of elementsToHighlight) {
            elementToHighlight.classList.add('lincs-mark-click-block');
            elementToHighlight.style.cssText = 'cursor: default;';

            const mark = document.createElement('mark');
            mark.style.backgroundColor = '#FFC2EC';
            const link = document.createElement('span');
            link.style.color = 'black';
            link.setAttribute('class', 'lincs-mark');
            link.setAttribute("data-lincs-mark", 'true');
            link.addEventListener('mouseover', (event) => UIController.getMatchedEntities(event, false, matchedURI));

            addLincsMark(elementToHighlight.childNodes);
            link.innerHTML = elementToHighlight.innerHTML;
            elementToHighlight.innerHTML = "";

            mark.appendChild(link);
            elementToHighlight.appendChild(mark);
            Globals.numHighlights += 1;
          }
        }
      }
      resolve();
    });
  });
}

function scanTextNodes(scanElement: Node, scanId: string) {
  const walk = document.createTreeWalker(scanElement, NodeFilter.SHOW_TEXT, null);
  delayRequest(walk, null, null, scanId).then(() => {
    Promise.all(Globals.inFlightRequests).then(() => {
      if (Globals.loading && Globals.scanId == scanId) {
        Globals.loading = false;

        Globals.highlightElements = Array.from(document.querySelectorAll(".lincs-mark"));
        Globals.highlightElements = Globals.highlightElements.filter(element => {
          return element.innerText != "";
        });

        Globals.sidebar.contentWindow.postMessage(
          { type: "done-highlights", highlights: Globals.highlightElements.map(element => element.innerText) },
          "*"
        );
      }
    });
  });
}

/**
 * Creates a batch of text to send for scanning
 * @param {*} walk the document tree walker
 * @param {*} searchText the string to add the batch to
 * @param {*} nodes a list of nodes that are going to be scanned
 * @return the leftover node at the end of the batch
 */
function createSearchBatch(walk: { nextNode: () => any; }, nodes: any[], currentSearchTextLength: number) {
  let searchText = '';
  let node = walk.nextNode();
  while (node) {
    if (node != null && node.data.trim().length > 1 && !['SCRIPT', 'STYLE', 'NOSCRIPT'].includes(node.parentNode.tagName)) {
      if (node.parentElement != null && !(node.parentElement.offsetWidth || node.parentElement.offsetHeight || node.parentElement.getClientRects().length)) {
        node = walk.nextNode();
        continue;
      }
      if (searchText.length + node.nodeValue.trim().length > Constants.textBatchSize - currentSearchTextLength) {
        return [searchText, node];
      }
      nodes.push(node);
      Globals.allNodes.push(node);
      searchText += ' ' + node.nodeValue.trim() + ' ';
    }
    node = walk.nextNode();
  }
  return [searchText, null];
}

/**
 * Process the response provided after scanning a batch
 * @param {*} response the response from the batch scanner
 * @param {*} nodes a list of nodes that are going to be scanned
 * @param {*} resolveScan the promise to resolve when processing is complete
 * @return higlighted text in the DOM and a resolved promise
 */
async function processFinishedBatch(response: string[], nodes: any[], resolveScan: { (value: any): void; (): void; }, scanId: string) {
  const isFirstOccurrence = await new Promise((resolve) => {
    MessageController.getSettings().then((response: any) => {
      resolve(response.activeSettings.highlight.settings.displayFirstHighlight.value);
    });
  });
  if (Globals.loading && Globals.scanId == scanId) {
    Globals.finishedRequests += 1;
    for (const word of response) {
      if (isFirstOccurrence) {
        // Highlight only the first instance of a word
        if (!Globals.highlightedWords.includes(word)) {
          Globals.highlightedWords.push(word);
          highlightWordOccurrence(word, Globals.allNodes, true); // Highlight the first word instance
        }
      } else {
        highlightWordOccurrence(word, nodes, false); // Highlight all the matched words
      }
    }

    const locale = Globals.locale ? "en" : "fr";
    Globals.sidebar.contentWindow?.location.replace(Constants.sidebarURL + locale + "/entity-select/" + Globals.numHighlights + "/" + Math.round(Globals.finishedRequests / Globals.totalRequests * 100));
    resolveScan();
  }
}

/**
 * Determines whether delayRequest needs to be called for the next batch
 * @param {*} promiseResolve the promiseResolve passed into deplayRequest
 * @param {*} walk the document tree walker
 * @param {*} leftoverNode a node left over that is larger than the allowed batch size
 * @return a newly dispatched delayRequest call or a promise resolve
 */
function dispatchNextRequest(promiseResolve: (() => void) | null, walk: TreeWalker, leftoverNode: null, scanId: string): Promise<void> {
  if (Globals.loading && Globals.scanId == scanId) {
    if (promiseResolve == null) { // First time dispatching
      return new Promise<void>((resolve) => { // Create promise first time through
        if (walk.nextNode()) {
          setTimeout(() => {
            Globals.totalRequests += 1;
            delayRequest(walk, leftoverNode, resolve, scanId);
          }, Constants.requestDelay);
        } else {
          resolve();
        }
      });
    } else { // Subsequent dispatching
      if (walk.nextNode() || leftoverNode != null) {
        setTimeout(() => {
          Globals.totalRequests += 1;
          delayRequest(walk, leftoverNode, promiseResolve, scanId);
        }, Constants.requestDelay);
      } else {
        promiseResolve(); // Resolve promise last time through (When all requests have been dispatched, but not necessarily finished)
      }
    }
  }
  return Promise.resolve();
}

/**
 * Makes scan requests to the backend, adding delay according to the requestDelay value
 * @param {*} walk tree walker for all text nodes in the document
 * @param {*} extraNode left over node that fit outside of the scan size
 * @param {*} promiseResolve the resolve callback for the promise triggered when all requests for a page are dispatched
 * @return a promise identifying when all requests have been initialized
 */
function delayRequest(walk: TreeWalker, extraNode: { nodeValue: string; } | null, promiseResolve: (() => void) | null, scanId: string): Promise<void> {
  const nodes = [] as any[];
  let searchText = '';

  // Handle leftover text from previous batch
  if (extraNode != null) {
    nodes.push(extraNode);
    Globals.allNodes.push(extraNode);
    searchText += ' ' + extraNode.nodeValue.trim() + ' ';
  }

  const searchBatch = createSearchBatch(walk, nodes, searchText.length);
  searchText += searchBatch[0];
  const leftoverNode = searchBatch[1];

  Globals.inFlightRequests.push(new Promise<void>((resolve) => {
    MessageController.sendMessage({ type: 'start-scan', text: searchText }).then(async (response: any) => {
      if (response && response[0] != "LINCS-ERROR") {
        await processFinishedBatch(response, nodes, resolve, scanId);
      } else {
        Globals.finishedRequests += 1;
        const locale = Globals.locale ? "en" : "fr";
        Globals.sidebar.contentWindow?.location.replace(Constants.sidebarURL + locale + "/entity-select/" + Globals.numHighlights + "/" + Math.round(Globals.finishedRequests / Globals.totalRequests * 100));
        resolve();
      }
    });
  }));

  return dispatchNextRequest(promiseResolve, walk, leftoverNode, scanId);
}

/**
 * Replaces all words found in the triple store with highlighted a tags
 * @param {*} text the parent node to search under
 * @param {*} find the regex expression of what to look for
 * @param {*} replaceMatch a function callback to replace the found entries
 * @return highlights in the DOM
 */
function replaceInText(text: any, find: RegExp, replaceMatch: { (match: any): HTMLElement; (arg0: any): any; }, highlightFirst = false) {
  if (!text.nodeValue) return false;
  if (text.parentNode.hasAttribute("data-lincs-mark")) return false;

  const matches = [];
  let match = find.exec(text.nodeValue);
  while (match) {
    matches.push(match);
    if (highlightFirst) { // If highlighting just the first, break loop on first match
      break;
    }
    match = find.exec(text.nodeValue);
  }

  for (let i = matches.length - 1; i >= 0; i--) {
    const match = matches[i];
    text.splitText(match.index);
    text.nextSibling.splitText(match[0].length);

    const newNode = replaceMatch(match);
    text.parentNode.replaceChild(newNode, text.nextSibling);

    const nearestLink = newNode.closest('a');
    if (nearestLink) {
      nearestLink.classList.add('lincs-mark-click-block');
      nearestLink.style.cssText = 'cursor: default;';
    }
  }
  return (matches.length > 0);
}

/** 
 * Highlight words on the basis of isFirstOccurrence
 * @param {*} word to find on the page
 * @param {*} nodeArr to search all the nodes
 * @param {*} isFirstOccurrence to check if the toggle to display first occurrence is on/off 
 * @return highlights in the DOM
 */
function highlightWordOccurrence(word: string, nodeArr: any[], isFirstOccurrence: boolean | undefined) {
  // regex word boundary \b doesn't work with special/accented characters, e.g. é ë
  // rather check for longer words (3 or more characters) to filter out short names
  if (word.length < 3) return;
  const find = new RegExp(word?.replace(/([|])/g, '\\$1'), 'gi');
  let highlightMade = false;
  for (let searchNode of nodeArr) {
    do {
      highlightMade = replaceInText(searchNode, find, (match) => {
        const mark = document.createElement('mark');
        mark.style.backgroundColor = '#C2E5FF';
        const link = document.createElement('span');
        link.style.color = 'black';
        link.setAttribute('class', 'lincs-mark');
        link.setAttribute("data-lincs-mark", 'true');
        link.addEventListener('mouseover', UIController.getMatchedEntities);
        link.appendChild(document.createTextNode(match[0]));
        mark.appendChild(link);
        Globals.numHighlights += 1;
        return mark;
      }, isFirstOccurrence)

      if (highlightMade && isFirstOccurrence) {
        break;
      }
      searchNode = searchNode.nextSibling;
    } while (searchNode)

    if (highlightMade && isFirstOccurrence) {
      break;
    }
  }
}

function addLincsMark(nodeList: any) {
  for (const node of nodeList) {
    addLincsMark(node.childNodes);

    if (node.classList != null) {
      node.classList.add('lincs-mark-click-block');
      node.setAttribute("data-lincs-mark", 'true');
    }
  }
}