/* eslint-disable @typescript-eslint/no-explicit-any */
import * as BubblesDisplayService from "@ce-content/BubblesDisplayService";
import * as Constants from "@ce-content/Constants";
import { EntityNode, Entity, Globals, Predicate, Entities } from "@ce-content/Globals";

// The message controller is swapped depending on the build
let MessageController: any = null;
import(
  process.env.BUILD_ENV == 'PLUGIN' ? 
  "@/packages/content/PluginMessageController" : 
  "@/packages/content/LibraryMessageController"
).then((controller) => {MessageController = controller});

/**
* Retrieves data for an entity to display in the sidebar and bubbles
* @param {*} request the incoming request from the message listener
* @param {*} addToEntityList boolean to add to the entityList
* @return data required to display entity info in nodes and the sidebar
*/
function showEntityGraph(request: Entity, addToEntityList: boolean) {
  let label = request.label;
  if (label == null) {
    label = request.uri;
  }
  if (label.length > 27) {
    label = label.substring(0, 26) + "...";
  }

  if (addToEntityList) {
    if (Globals.entityList.length >= 1) {
      Globals.d3Data.mainContainer[1].select('#title-bubble-container').attr('visibility', 'hidden');
    }
    Globals.entityList.unshift(request);
  }

  Globals.currentEntity = {
    uri: request.uri,
    label: label,
    page: 1,
    displayedCategory: "All",
    predicates: {}
  }
  const locale = Globals.locale ? "en" : "fr";
  Globals.sidebar.contentWindow.location.replace(Constants.sidebarURL + locale + "/entity-info/" + encodeURIComponent(JSON.stringify(Globals.entityList.map(entity => entity.uri))));
  Globals.d3Data.mainContainer[0].select('#main-text').text(translateText("Loading...", Globals.locale));

  if (!addToEntityList) {
    Globals.sidebar.contentWindow.postMessage(Globals.entityApiList[0], "*");

    if (Globals.entityList.length < 2) {
      Globals.currentEntity.displayedCategory = 'Selection';
    }

    if (Globals.entityApiList[0].type == "send-entity-info") {
      setupEntityGraph(processGetEntityInfo(Globals.entityApiList[0].response));
    } else {
      setupEntityGraph(processGetRelatedEntities(Globals.entityApiList[0].response));
    }
    return;
  }

  let message = {};
  if (Globals.entityList.length == 1 || Globals.entityList.length == 3) {
    message = { type: 'get-entity-info', uri: request.uri };
  } else if (Globals.entityList.length == 2) {
    message = { type: 'get-related-entities', firstURI: Globals.entityList[0].uri, secondURI: Globals.entityList[Globals.entityList.length - 1].uri };
  }
  MessageController.sendMessage(message).then((response: any) => {
    parseEntities(message, response);
  });
}

/**
 * Draws the entities in a specific category when a user selects the category
 * @param {*} category the category to draw
 * @return bubbles displaying the contents of a specific category
 */
function drawCategoryEntities(category: string) {
  category = translateText(category, true);

  Globals.currentEntity.displayedCategory = category;
  if (category != 'Selection') {
    Globals.d3Data.mainContainer[0].select('#title-text').text(translateText(category, Globals.locale));
    Globals.currentEntity.page = 1;
    BubblesDisplayService.removeEntities();
    BubblesDisplayService.setupPagination();

    const predicates = Globals.currentEntity.predicates[Globals.currentEntity.displayedCategory];
    const nodes = predicates.predicates.slice(0, 5);
    
    BubblesDisplayService.drawEntities(nodes);
    BubblesDisplayService.drawBackButton();

  } else {
    BubblesDisplayService.removeEntities();
    BubblesDisplayService.drawCategories();
  }
}

/**
 * Updates the focussed entity
 * @param {*} entity the entity object of the newly selected entity
 * @return a new entity is added to the chain
 */
function addNewFocus(entity: Entity) {
  BubblesDisplayService.removeEntities();
  Globals.currentEntity = {
    uri: "http://id.lincsproject.ca",
    label: translateText("Loading...", Globals.locale),
    page: 1,
    predicates: {},
    displayedCategory: "",
  }

  const target = Globals.currentTarget.getBoundingClientRect();
  BubblesDisplayService.drawEntitySetup(target);
  showEntityGraph(entity, true);
}

/**
 * Pops the current entity, then updates the focussed entity
 * @param {*} entity the entity object of the newly selected entity
 * @return a new entity is added to the chain
 */
function removeAndAddNewFocus(entity: Entity) {
  BubblesDisplayService.removeEntities();
  popNumEntities(1);
  addNewFocus(entity);
}

/**
 * Returns to the previous focussed entity
 * @return an entity is removed from the chain
 */
function entityTraversalBack(amount: number) {
  BubblesDisplayService.removeEntities();
  popNumEntities(amount);
  showEntityGraph(Globals.entityList[0], false);
}

/**
 * Removes a set number of entities from the top
 * @param {*} amount the amount of entities to pop
 * @return the amount of entities requests are removed from the chain
 */
function popNumEntities(amount: number, popFromEntityList = true) {
  for (let i = 0; i < amount; i++) {
    popEntity(popFromEntityList);
  }
}

function translateText(category: string, locale: boolean) {
  switch (category) {
    case "People":
    case "Personnes":
      return locale ? "People" : "Personnes";
    case "Groups":
    case "Groupes":
      return locale ? "Groups" : "Groupes";
    case "Places":
    case "Lieux":
      return locale ? "Places" : "Lieux";
    case "Creative Works":
    case "Œuvres Créatives":
      return locale ? "Creative Works" : "Œuvres Créatives";
    case "Concepts":
      return locale ? "Concepts" : "Concepts";
    case "Web References":
    case "Références Web":
      return locale ? "Web References" : "Références Web";
    case "Other":
    case "Autre":
      return locale ? "Other" : "Autre";
    case "Select a matching entity":
    case "Sélectionnez une entité":
      return locale ? "Select a matching entity" : "Sélectionnez une entité";
    case "Loading...":
    case "Chargement...":
      return locale ? "Loading..." : "Chargement...";
    case "Connections":
    case "Connexions":
      return locale ? "Connections" : "Connexions";
    case "Categories":
    case "Catégories":
      return locale ? "Categories" : "Catégories";
    default:
      return category
  }
}

export { showEntityGraph, drawCategoryEntities, addNewFocus, removeAndAddNewFocus, entityTraversalBack, popNumEntities, translateText };

/** Helper Functions */

/**
 * Parse incoming entities from the background script
 * @param {*} message the message to the background script
 * @param {*} response the response from the background script
 * @return lazy loading of extra data, sends info to sidebar, and triggers graph update
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function parseEntities(message: { type?: string; }, response: Entities) {
  if (message.type == "get-entity-info") {
    if (Globals.entityList.length == 3) {
      response.connectedEntities = response.mainEntityInfo.outgoing;
    }

    if (response.mainEntityInfo.outgoing != null) response = preprocessIncomingEntities(response);
    response = getImagesForEntities(response);

    if (Globals.entityList.length > 2) {
      Globals.currentEntity.displayedCategory = 'All';
    } else {
      Globals.currentEntity.displayedCategory = 'Selection';
    }

    if (Globals.entityList.length == 1) {
      loadAnnotationInfo(response.mainEntityInfo.resource).then((annotations) => {
        if (annotations) {
          response.connectedEntities = response.connectedEntities.concat(annotations);
        }

        Globals.entityApiList.unshift({ type: "send-entity-info", response: response });
        Globals.sidebar.contentWindow.postMessage({ type: "send-entity-info", response: response }, "*");
        lazyLoadExtraData(response, true);

        setupEntityGraph(processGetEntityInfo(response));
      });
    } else {
      Globals.entityApiList.unshift({ type: "send-entity-info", response: response });
      Globals.sidebar.contentWindow.postMessage({ type: "send-entity-info", response: response }, "*");
      lazyLoadExtraData(response, true);

      setupEntityGraph(processGetEntityInfo(response));
    }
  } else {
    MessageController.sendMessage({ type: 'get-entity-info', uri: Globals.entityList[0].uri }).then((response2: any) => {
      response2.connectedEntities = response;
      if (response2.mainEntityInfo.outgoing != null) response2 = preprocessIncomingEntities(response2);
      response2 = getImagesForEntities(response2);

      Globals.entityApiList.unshift({ type: "send-related-entities", response: response2 });
      Globals.sidebar.contentWindow.postMessage({ type: "send-related-entities", response: response2 }, "*");

      lazyLoadExtraData(response2, false);
      Globals.currentEntity.displayedCategory = 'All';

      setupEntityGraph(processGetRelatedEntities(response2));
    });
  }
}

/**
 * Query for web references related to a specific uri
 * @param {*} uri the uri that the annoations are connected to
 */
function loadAnnotationInfo(uri: string) {
  return new Promise((resolve) => {
    const locale = Globals.locale ? "en" : "fr";
    fetch(Constants.lincsApiURL + "entityAnnotations", {
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': Constants.authToken,
      },
      method: "POST",
      body: JSON.stringify({
        uri: uri,
        language: locale
      }),
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw new Error("FAILURE");
      })
      .then((response) => {
        response.forEach((annotation: EntityNode) => {
          annotation.resource = annotation.annotation;
          annotation.resourceLabel = annotation.annotationLabel;
          annotation.type = "http://www.cidoc-crm.org/cidoc-crm/E33_Linguistic_Object";
          annotation.typeLabel = "Annotation";
          annotation.objectIsURI = true;
        });

        resolve(response);
      })
      .catch(() => {
        console.log("Query Error");
        resolve(false);
      });
  });
}

/**
 * This is a function that selects which type of data processing to use
 * @param {*} isEntityInfo a boolean telling us if this is for get-entity-info, defaulting to get-related-entities if not
 * @param {*} entities the entities to process
 * @return the processed entities
 */
function processEntities(isEntityInfo: boolean, entities: any) {
  if (isEntityInfo) {
    return processGetEntityInfo(entities);
  }
  return processGetRelatedEntities(entities);
}

/**
 * Lazy loads extra information needed by the sidebar and bubble display
 * @param {*} entities the entities to load extra info for
 * @return extra information appended to the entities and update message sent to sidebar
 */
function lazyLoadExtraData(entities: Entities, isEntityInfo: boolean) {
  getPersonInfo(entities)?.then((personInfoResponse) => {
    if (personInfoResponse != null) {
      Globals.entityApiList[0].response = entities;
      Globals.sidebar.contentWindow.postMessage({ type: "update-entity-info", response: entities }, "*");

      Globals.currentEntity.predicates = sortPredicateCategories(processEntities(isEntityInfo, personInfoResponse));
      BubblesDisplayService.redrawNodes();
    }

    getWikidataPersonInfo(entities, entities.mainEntityInfo.usesDefaultImage).then((wikidataInfoResponse) => {
      if (wikidataInfoResponse != null) {
        Globals.entityApiList[0].response = entities;
        Globals.sidebar.contentWindow.postMessage({ type: "update-entity-info", response: entities }, "*");

        Globals.currentEntity.predicates = sortPredicateCategories(processEntities(isEntityInfo, wikidataInfoResponse));
        BubblesDisplayService.redrawNodes();
      }
    });
  });
  getOutgoingPersonInfo(entities).then((outgoingPersonResponse) => {
    Globals.entityApiList[0].response = entities;
    Globals.sidebar.contentWindow.postMessage({ type: "update-entity-info", response: entities }, "*");

    Globals.currentEntity.predicates = sortPredicateCategories(processEntities(isEntityInfo, outgoingPersonResponse));
    BubblesDisplayService.redrawNodes();

    getOutgoingWikidataPersonInfo(entities).then((outgoingWikidataResponse) => {
      Globals.entityApiList[0].response = entities;
      Globals.sidebar.contentWindow.postMessage({ type: "update-entity-info", response: entities }, "*");

      Globals.currentEntity.predicates = sortPredicateCategories(processEntities(isEntityInfo, outgoingWikidataResponse));
      BubblesDisplayService.redrawNodes();
    });
  });
  getPlaceInfo(entities).then((placeInfoResponse) => {
    if (placeInfoResponse != null) {
      Globals.entityApiList[0].response = entities;
      Globals.sidebar.contentWindow.postMessage({ type: "update-entity-info", response: entities }, "*");

      Globals.currentEntity.predicates = sortPredicateCategories(processEntities(isEntityInfo, placeInfoResponse));
      BubblesDisplayService.redrawNodes();
    }
  });
  getOutgoingPlaceInfo(entities).then((outgoingPlaceResponse) => {
    Globals.entityApiList[0].response = entities;
    Globals.sidebar.contentWindow.postMessage({ type: "update-entity-info", response: entities }, "*");

    Globals.currentEntity.predicates = sortPredicateCategories(processEntities(isEntityInfo, outgoingPlaceResponse));
    BubblesDisplayService.redrawNodes();
  });
  getGroupInfo(entities).then((groupInfoResponse) => {
    if (groupInfoResponse != null) {
      Globals.entityApiList[0].response = entities;
      Globals.sidebar.contentWindow.postMessage({ type: "update-entity-info", response: entities }, "*");

      Globals.currentEntity.predicates = sortPredicateCategories(processEntities(isEntityInfo, groupInfoResponse));
      BubblesDisplayService.redrawNodes();
    }
  });
  getWorkInfo(entities).then((workInfoResponse) => {
    if (workInfoResponse != null) {
      Globals.entityApiList[0].response = entities;
      Globals.sidebar.contentWindow.postMessage({ type: "update-entity-info", response: entities }, "*");

      Globals.currentEntity.predicates = sortPredicateCategories(processEntities(isEntityInfo, workInfoResponse));
      BubblesDisplayService.redrawNodes();
    }
  });
}

/**
 * Batches uris together to cut down on the number of requests
 * @param preds the predicates to batch
 * @param restrictedType the type to restrict the predicates to
 * @param batchSize the size of each batch
 * @return a batch of uris
 */
function batchInfoRequest(preds: Array<any>, restrictedType: string, wikidataFilter: string|null = null, batchSize: number = 10) {
  let batches = [];
  let currentBatch: Array<any> = [];

  for (const outgoingPred of preds) {
    if (outgoingPred.type == restrictedType) {
      let urisToCheck: Array<string> = [];
      if (wikidataFilter) {
        urisToCheck = [].concat(outgoingPred.sameAs, outgoingPred.object);
      } else {
        urisToCheck = [].concat(outgoingPred.object);
      }

      for (const uri of urisToCheck) {
        if (uri != null && (!wikidataFilter || uri.includes(wikidataFilter))) {
          currentBatch.push(uri);
          if (currentBatch.length >= batchSize) {
            batches.push(JSON.parse(JSON.stringify(wikidataFilter ? { linkType: wikidataFilter, batch: currentBatch } : currentBatch)));
            currentBatch = [];
          }
        }
      }
    }
  }

  if (currentBatch.length != 0) {
    batches.push(wikidataFilter ? { linkType: wikidataFilter, batch: currentBatch } : currentBatch);
  }
  return batches;
}

/**
 * Display info from showEntityGraph as nodes on the bubble display
 * @param {*} response a request from the sidebar to display nodes for a specific entity in the chain
 * @returns a bubble display for a specific entity in the chain
 */
function setupEntityGraph(response: Entity[]) {
  Globals.d3Data.mainContainer[0].select('#main-text').text(Globals.currentEntity.label);
  Globals.d3Data.mainContainer[0].select('#title-bubble-container').attr('visibility', null);

  Globals.currentEntity.predicates = sortPredicateCategories(response);

  if (Globals.currentEntity.displayedCategory == 'All') {
    Globals.d3Data.mainContainer[0].select('#title-text').text(translateText("Connections", Globals.locale));
    BubblesDisplayService.setupPagination();
    BubblesDisplayService.drawBackButton();
    BubblesDisplayService.drawEntities(Globals.currentEntity.predicates[Globals.currentEntity.displayedCategory].predicates.slice(0, 5));
  } else {
    BubblesDisplayService.drawCategories();
  }
}

/**
 * Sorts the given entities into categories
 * @param {*} entities a list of entities that need to be sorted by type
 * @return a list of entities sorted by their type
 */
function sortPredicateCategories(entities: EntityNode[]) {
  const categorizedPredicates: Record<string, Predicate> = {};
  categorizedPredicates['People'] = { maxPage: 0, predicates: [] };
  categorizedPredicates['Groups'] = { maxPage: 0, predicates: [] };
  categorizedPredicates['Places'] = { maxPage: 0, predicates: [] };
  categorizedPredicates['Creative Works'] = { maxPage: 0, predicates: [] };
  categorizedPredicates['Concepts'] = { maxPage: 0, predicates: [] };
  categorizedPredicates['Web References'] = { maxPage: 0, predicates: [] };
  categorizedPredicates['Other'] = { maxPage: 0, predicates: [] };
  categorizedPredicates['All'] = { maxPage: 0, predicates: [] };

  for (const entity of entities) {
    if (entity) {
      if (entity.type == "http://www.cidoc-crm.org/cidoc-crm/E21_Person") {
        categorizedPredicates['People'].predicates.push(entity);
      } else if (entity.type == "http://www.cidoc-crm.org/cidoc-crm/E74_Group" || entity.type == "http://www.cidoc-crm.org/cidoc-crm/E39_Actor") {
        categorizedPredicates['Groups'].predicates.push(entity);
      } else if (entity.type == "http://www.cidoc-crm.org/cidoc-crm/E53_Place") {
        categorizedPredicates['Places'].predicates.push(entity);
      } else if (entity.type == "http://www.cidoc-crm.org/cidoc-crm/E22_Human-Made_Object" || entity.type == "http://iflastandards.info/ns/fr/frbr/frbroo/F1_Work") {
        categorizedPredicates['Creative Works'].predicates.push(entity);
      } else if (entity.type == "http://www.w3.org/2004/02/skos/core#Concept") {
        categorizedPredicates['Concepts'].predicates.push(entity);
      } else if (entity.type == "http://www.cidoc-crm.org/cidoc-crm/E33_Linguistic_Object") {
        categorizedPredicates['Web References'].predicates.push(entity);
      } else {
        categorizedPredicates['Other'].predicates.push(entity);
      }
      categorizedPredicates['All'].predicates.push(entity);
    }
  }

  categorizedPredicates['People'].maxPage = Math.ceil(categorizedPredicates['People'].predicates.length / 5);
  categorizedPredicates['Groups'].maxPage = Math.ceil(categorizedPredicates['Groups'].predicates.length / 5);
  categorizedPredicates['Places'].maxPage = Math.ceil(categorizedPredicates['Places'].predicates.length / 5);
  categorizedPredicates['Creative Works'].maxPage = Math.ceil(categorizedPredicates['Creative Works'].predicates.length / 5);
  categorizedPredicates['Concepts'].maxPage = Math.ceil(categorizedPredicates['Concepts'].predicates.length / 5);
  categorizedPredicates['Web References'].maxPage = Math.ceil(categorizedPredicates['Web References'].predicates.length / 5);
  categorizedPredicates['Other'].maxPage = Math.ceil(categorizedPredicates['Other'].predicates.length / 5);
  categorizedPredicates['All'].maxPage = Math.ceil(categorizedPredicates['All'].predicates.length / 5);

  return categorizedPredicates;
}

/**
 * Process the output of get-entity-info for node display
 * @param {*} response 
 * @return a processed array of entities
 */
function processGetEntityInfo(response: Entities) {
  let output = [];

  // Square(data predicates) bubbles processing
  if (response.mainEntityInfo.outgoing != null) {
    output = JSON.parse(JSON.stringify(response.mainEntityInfo.outgoing)).filter((triple: { predicate: string; }) => {
      return (Globals.entityList.length > 2 && triple.predicate != "http://www.w3.org/2000/01/rdf-schema#label") || triple.predicate == "http://www.cidoc-crm.org/cidoc-crm/P2_has_type" || triple.predicate == "http://www.cidoc-crm.org/cidoc-crm/P3_has_note";
    }).map((triple: { objectIsURI: boolean; }) => {
      triple.objectIsURI = false;
      return triple;
    });
  }

  // Circle(object predicates) bubbles processing
  if (Globals.entityList.length <= 2 && response.connectedEntities != null) {
    output = output.concat(JSON.parse(JSON.stringify(response.connectedEntities)).map((entity: { objectIsURI: boolean; }) => {
      entity.objectIsURI = true;
      return entity;
    }));
  }

  return output;
}

/**
 * Process the output of get-related-entities for node display
 * @param {*} response 
 * @return a processed array of entities
 */
function processGetRelatedEntities(response: Entities) {
  return JSON.parse(JSON.stringify(response.connectedEntities)).filter((entity: { outgoing: any; }) => {
    for (const triple of entity.outgoing) {
      if (triple.object == "http://www.w3.org/ns/oa#Annotation" || triple.object == "http://www.w3.org/ns/oa#identifying") {
        return false;
      }
    }
    return true;
  }).map((entity: { objectIsURI: boolean; }) => {
    entity.objectIsURI = true;
    return entity;
  });
}

/**
 * Process incoming entities by combining equivalent predicates
 * @param {*} response 
 * @return a modified response
 */
function preprocessIncomingEntities(response: Entities) {
  for (let i = 0; i < response.mainEntityInfo.outgoing.length; i++) {
    const outgoingPred = response.mainEntityInfo.outgoing[i];

    if (outgoingPred.predicateLabel == "hasBody") {
      for (const [index, searchOutgoingPred] of response.mainEntityInfo.outgoing.entries()) {
        if (searchOutgoingPred.object == outgoingPred.object && searchOutgoingPred.predicateLabel == "refers to") {
          response.mainEntityInfo.outgoing.splice(index, 1);
          if (index < i) i--;
        }
      }
    } else if (outgoingPred.predicateLabel == "carried out by") {
      for (const [index, searchOutgoingPred] of response.mainEntityInfo.outgoing.entries()) {
        if (searchOutgoingPred.object == outgoingPred.object && searchOutgoingPred.predicateLabel == "had participant") {
          response.mainEntityInfo.outgoing.splice(index, 1);
          if (index < i) i--;
        }
      }
    } else if (outgoingPred.predicateLabel == "has type") {
      for (const [index, searchOutgoingPred] of response.mainEntityInfo.outgoing.entries()) {
        if (searchOutgoingPred.object == outgoingPred.object && searchOutgoingPred.predicateLabel == "motivatedBy") {
          response.mainEntityInfo.outgoing.splice(index, 1);
          if (index < i) i--;
        }
      }
    } else if (outgoingPred.predicateLabel == "was attributed by") {
      response.mainEntityInfo.outgoing.splice(i, 1);
    } else if (outgoingPred.predicate == "http://id.lincsproject.ca/lincs/connectionCount") {
      response.mainEntityInfo.outgoing.splice(i, 1);
    }
  }

  return response;
}

/**
 * Retrieves the image of an entity for easier access
 * @param {*} response 
 * @return a response with images attached
 */
function getImagesForEntities(response: Entities) {
  let image;
  let hasImage = false;
  if (response.mainEntityInfo.outgoing) {
    for (const outgoingPred of response.mainEntityInfo.outgoing) {
      if (outgoingPred.type == "http://www.cidoc-crm.org/cidoc-crm/E36_Visual_Item") {
        image = outgoingPred.object;
        hasImage = true;
        break;
      }
    }
  }

  // Get default image if none exist
  if (!hasImage) {
    if (response.mainEntityInfo.typeLabel) {
      const iconString: string = response.mainEntityInfo.typeLabel.replace(" ", "_").toUpperCase();
      const path = Constants.iconMapper[iconString] || "default.svg";
      image = Constants.pluginURL + "cidoc-images/" + path;
    } else {
      image = Constants.pluginURL + "cidoc-images/default.svg";
    }
  }

  response.mainEntityInfo.image = image;
  response.mainEntityInfo.usesDefaultImage = !hasImage;
  return response;
}

/**
 * Checks to see what fields a person entity is missing so that wikidata knows what to query for
 * @param {*} response 
 * @return a tuple containg the fields the main entity is missing
 */
function checkForMissingPersonInfo(response: Entities) {
  let hasDescription = false;
  let hasBirthDate = false;
  let hasBirthPlace = false;
  let hasDeathDate = false;
  let hasDeathPlace = false;
  let externalLink = null;

  for (const outgoingPred of response.mainEntityInfo.outgoing) {
    if (outgoingPred.predicateLabel == "has note") {
      hasDescription = true;
    } else if (outgoingPred.predicate == "birthDate") {
      hasBirthDate = true;
    } else if (outgoingPred.predicate == "birthPlace") {
      hasBirthPlace = true;
    } else if (outgoingPred.predicate == "deathDate") {
      hasDeathDate = true;
    } else if (outgoingPred.predicate == "deathPlace") {
      hasDeathPlace = true;
    } else if (outgoingPred.predicate == "http://www.w3.org/2002/07/owl#sameAs" && (outgoingPred.object.includes("wikidata") || outgoingPred.object.includes("viaf") || outgoingPred.object.includes("getty"))) {
      externalLink = outgoingPred.object;
    }
  }

  return [externalLink, hasDescription, hasBirthDate, hasBirthPlace, hasDeathDate, hasDeathPlace];
}

/**
 * Gets person info for main entities without full data
 * @param {*} response 
 * @return a promise of when the person request is complete
 */
function getPersonInfo(response: any) {
  return new Promise((resolve) => {
    if (response.mainEntityInfo.type == "http://www.cidoc-crm.org/cidoc-crm/E21_Person") {
      const locale = Globals.locale ? "en" : "fr";
      fetch(Constants.lincsApiURL + "person/statements", {
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          'Authorization': Constants.authToken,
        },
        method: "POST",
        body: JSON.stringify({
          uris: [response.mainEntityInfo.resource],
          language: locale
        }),
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          }
          throw new Error("FAILURE");
        })
        .then((personResponse) => {
          personResponse = personResponse[0].statements;
          personResponse = personResponse.filter((statement: any) => {
            let type = statement.type;
            return (type == "attribute" || type == "birthDate" || type == "birthPlace" || type == "deathDate" || type == "deathPlace" || type == "group" || type == "pursuit" || type == "work");
          }).map((statement: any) => {
            let type = statement.type;
            let objectIsURI = !(type == "birthDate" || type == "birthPlace" || type == "deathDate" || type == "deathPlace");
            
            return {
              graph: statement.graph,
              predicate: statement.type,
              objectIsURI: objectIsURI,
              object: statement.uri,
              objectLabel: statement.value
            };
          });
          response.mainEntityInfo.outgoing = response.mainEntityInfo.outgoing.concat(personResponse);

          resolve(response);
        })
        .catch(() => {
          console.log("Query Error");
          resolve(null);
        });
    } else {
      resolve(null);
    }
  });
}

/**
 * Gets wikidata info for main entities without full data
 * @param {*} response 
 * @param {*} usesDefaultImage 
 * @return a promise of when the wikidata request is complete
 */
function getWikidataPersonInfo(response: any, usesDefaultImage: boolean) {
  let hasDescription = false;
  let hasBirthDate = false;
  let hasBirthPlace = false;
  let hasDeathDate = false;
  let hasDeathPlace = false;
  let externalLink: any = null;

  if (response.mainEntityInfo.outgoing) {
    const requiredInfo = checkForMissingPersonInfo(response);
    externalLink = requiredInfo[0];
    hasDescription = requiredInfo[1];
    hasBirthDate = requiredInfo[2];
    hasBirthPlace = requiredInfo[3];
    hasDeathDate = requiredInfo[4];
    hasDeathPlace = requiredInfo[5];

    const resource = response.mainEntityInfo.resource;
    if (resource.includes("wikidata") || resource.includes("viaf") || resource.includes("getty")) {
      externalLink = resource;
    }
  }

  return new Promise((resolve) => {
    if (response.mainEntityInfo.type == "http://www.cidoc-crm.org/cidoc-crm/E21_Person" && (!hasDescription || !hasBirthDate || !hasBirthPlace || !hasDeathDate || !hasDeathPlace || usesDefaultImage) && externalLink != null) {
      let linkType = "";
      if (externalLink.includes("wikidata")) {
        linkType = "wikidata";
      } else if (externalLink.includes("viaf")) {
        linkType = "viaf";
      } else if (externalLink.includes("getty")) {
        linkType = "getty";
      }
      fetch(Constants.pluginURL + linkType + "?uris=" + encodeURIComponent(JSON.stringify([externalLink])), {
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          'Authorization': Constants.authToken,
        },
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          }
          throw new Error("FAILURE");
        })
        .then((wikiResponse) => {
          wikiResponse = wikiResponse[0];
          [hasDeathPlace] = checkForMissingPersonInfo(response);
          const attributionValue = Object.prototype.hasOwnProperty.call(wikiResponse, "person") ? wikiResponse.person.value : externalLink;

          if (usesDefaultImage && wikiResponse.image != null) {
            response.mainEntityInfo.image = wikiResponse.image.value;
            response.mainEntityInfo.imageWikidataAttribution = attributionValue;
            response.mainEntityInfo.imageName = Object.prototype.hasOwnProperty.call(wikiResponse, "imageLegend") ? wikiResponse.imageLegend.value : wikiResponse.image.value;
          }
          if (!hasDescription && wikiResponse.description != null) {
            response.mainEntityInfo.outgoing.push({
              predicateLabel: "has note",
              object: wikiResponse.description.value,
              wikidataAttribution: attributionValue,
              objectIsURI: false,
            });
          }
          if (!hasBirthDate && wikiResponse.date_of_birth != null) {
            response.mainEntityInfo.outgoing.push({
              predicateLabel: "Date of Birth",
              predicate: "birthDate",
              object: wikiResponse.date_of_birth.value.split("T")[0].replaceAll('-', '/'),
              wikidataAttribution: attributionValue,
              objectIsURI: false,
            });
          }
          if (!hasBirthPlace && wikiResponse.place_of_birth != null) {
            response.mainEntityInfo.outgoing.push({
              predicateLabel: "Place of Birth",
              predicate: "birthPlace",
              object: wikiResponse.place_of_birth.value,
              wikidataAttribution: attributionValue,
              objectIsURI: false,
            });
          }
          if (!hasDeathDate && wikiResponse.date_of_death != null) {
            response.mainEntityInfo.outgoing.push({
              predicateLabel: "Date of Death",
              predicate: "deathDate",
              object: wikiResponse.date_of_death.value.split("T")[0].replaceAll('-', '/'),
              wikidataAttribution: attributionValue,
              objectIsURI: false,
            });
          }
          if (!hasDeathPlace && wikiResponse.place_of_death != null) {
            response.mainEntityInfo.outgoing.push({
              predicateLabel: "Place of Death",
              predicate: "deathPlace",
              object: wikiResponse.place_of_death.value,
              wikidataAttribution: attributionValue,
              objectIsURI: false,
            });
          }

          resolve(response);
        })
        .catch(() => {
          console.log("Query Error");
          resolve(null);
        });
    } else {
      resolve(null);
    }
  });
}

/**
 * Retrieves extra information for outbound person entities
 * @param {*} response 
 * @return a promise of when all the persons requests are complete
 */
function getOutgoingPersonInfo(response: Entities): Promise<Entities> {
  const promises: Promise<boolean>[] = [];
  if (response.mainEntityInfo.outgoing) {
    let batches = batchInfoRequest(response.mainEntityInfo.outgoing, "http://www.cidoc-crm.org/cidoc-crm/E21_Person");
    for (const batch of batches) {
      promises.push(
        new Promise((resolve) => {
          const locale = Globals.locale ? "en" : "fr";
          fetch(Constants.lincsApiURL + "person", {
            headers: {
              "Accept": "application/json",
              "Content-Type": "application/json",
              'Authorization': Constants.authToken,
            },
            method: "POST",
            body: JSON.stringify({
              uris: batch,
              language: locale
            }),
          })
            .then((response) => {
              if (response.ok) {
                return response.json();
              }
              throw new Error("FAILURE");
            })
            .then((personResponse) => {
              for (const person of personResponse) {
                for (const outgoingPred of response.mainEntityInfo.outgoing) {
                  if (outgoingPred.object == person.uri) {
                    outgoingPred.image = person.image;
                    outgoingPred.birthDate = person.birthDate;
                    outgoingPred.birthPlace = person.birthPlace;
                    outgoingPred.deathDate = person.deathDate;
                    outgoingPred.deathPlace = person.deathPlace;
                    outgoingPred.sameAs = person.sameAs;
                    outgoingPred.description = person.note;
                  }
                }
              }

              resolve(true);
            })
            .catch(() => {
              console.log("Query Error");
              resolve(false);
            });
        }),
      );
    }
  }
  return new Promise((resolve) => {
    Promise.all(promises).then(() => {
      resolve(response);
    });
  });
}

/**
 * Retrieves extra information from wikidata for outbound person entities
 * @param {*} response 
 * @return a promise of when all the wikidata requests are complete
 */
function getOutgoingWikidataPersonInfo(response: Entities): Promise<Entities> {
  const promises: Promise<boolean>[] = [];
  if (response.mainEntityInfo.outgoing) {
    let batches = batchInfoRequest(response.mainEntityInfo.outgoing, "http://www.cidoc-crm.org/cidoc-crm/E21_Person", "wikidata", 3);
    batches = batches.concat(batchInfoRequest(response.mainEntityInfo.outgoing, "http://www.cidoc-crm.org/cidoc-crm/E21_Person", "viaf", 3));
    batches = batches.concat(batchInfoRequest(response.mainEntityInfo.outgoing, "http://www.cidoc-crm.org/cidoc-crm/E21_Person", "getty", 3));
    for (const batch of batches) { 
      promises.push(
        new Promise((resolve) => {
          fetch(Constants.pluginURL + batch.linkType + "?uris=" + encodeURIComponent(JSON.stringify(batch.batch)), {
            headers: {
              "Accept": "application/json",
              "Content-Type": "application/json",
              'Authorization': Constants.authToken,
            },
          })
            .then((response) => {
              if (response.ok) {
                return response.json();
              }
              throw new Error("FAILURE");
            })
            .then((wikiResponse) => {
              for (const person of wikiResponse) {
                for (const outgoingPred of response.mainEntityInfo.outgoing) {
                  if (outgoingPred.object == person.uri.value || outgoingPred.sameAs == person.uri.value) {
                    let wikidataInfoUsed = false;

                    if (outgoingPred.image == null && person.image != null) {
                      outgoingPred.image = person.image.value;
                      wikidataInfoUsed = true;
                    }
                    if (outgoingPred.description == null && person.description != null) {
                      outgoingPred.description = person.description.value;
                      wikidataInfoUsed = true;
                    }
                    if (outgoingPred.birthDate == null && person.date_of_birth != null) {
                      outgoingPred.birthDate = person.date_of_birth.value.split("T")[0].replaceAll('-', '/');
                      wikidataInfoUsed = true;
                    }
                    if (outgoingPred.birthPlace == null && person.place_of_birth != null) {
                      outgoingPred.birthPlace = person.place_of_birth.value;
                      wikidataInfoUsed = true;
                    }
                    if (outgoingPred.deathDate == null && person.date_of_death != null) {
                      outgoingPred.deathDate = person.date_of_death.value.split("T")[0].replaceAll('-', '/');
                      wikidataInfoUsed = true;
                    }
                    if (outgoingPred.deathPlace == null && person.place_of_death != null) {
                      outgoingPred.deathPlace = person.place_of_death.value;
                      wikidataInfoUsed = true;
                    }
                    if (wikidataInfoUsed) {
                      outgoingPred.wikidataAttribution = Object.prototype.hasOwnProperty.call(person, "person") ? person.person.value : person.uri;
                    }
                  }
                }
              }
              resolve(true);
            })
            .catch(() => {
              console.log("Query Error");
              resolve(false);
            });
        })
      );
    }
  }

  return new Promise((resolve) => {
    Promise.all(promises).then(() => {
      resolve(response);
    });
  });
}

/**
 * Gets place info for main entities
 * @param {*} response 
 * @return a promise of when the place request is complete
 */
function getPlaceInfo(response: Entities) {
  return new Promise((resolve) => {
    if (response.mainEntityInfo.type == "http://www.cidoc-crm.org/cidoc-crm/E53_Place") {
      const locale = Globals.locale ? "en" : "fr";
      fetch(Constants.lincsApiURL + "place/statements", {
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          'Authorization': Constants.authToken,
        },
        method: "POST",
        body: JSON.stringify({
          uris: [response.mainEntityInfo.resource],
          language: locale
        }),
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          }
          throw new Error("FAILURE");
        })
        .then((placeResponse) => {
          placeResponse = placeResponse[0].statements;
          placeResponse = placeResponse.filter((statement: any) => {
            let type = statement.type;
            return (type == "definedBy" || type == "bornHere" || type == "diedHere");
          }).map((statement: any) => { 
            if (statement.type == "definedBy") {
              let coords = statement.value.replace(
                "POINT(",
                "",
              );
              coords = coords.replace(")", "");
              coords = coords.split(" ");
              coords = [parseFloat(coords[1]), parseFloat(coords[0])];

              return {
                graph: statement.graph,
                predicate: "latAndLong",
                objectIsURI: false,
                object: coords
              };
            } else {
              return {
                graph: statement.graph,
                predicate: statement.type,
                objectIsURI: true,
                object: statement.uri,
                objectLabel: statement.value
              };
            }
          });
          response.mainEntityInfo.outgoing = response.mainEntityInfo.outgoing.concat(placeResponse);

          resolve(response);
        })
        .catch(() => {
          console.log("Query Error");
          resolve(false);
        });
    } else {
      resolve(null);
    }
  });
}

/**
 * Retrieves extra information for outbound place entities
 * @param {*} response 
 * @return a promise of when all the place requests are complete
 */
function getOutgoingPlaceInfo(response: Entities) {
  const promises: Promise<boolean>[] = [];
  if (response.mainEntityInfo.outgoing) {
    let batches = batchInfoRequest(response.mainEntityInfo.outgoing, "http://www.cidoc-crm.org/cidoc-crm/E53_Place");
    for (const batch of batches) {
      promises.push(
        new Promise((resolve) => {
          const locale = Globals.locale ? "en" : "fr";
          fetch(Constants.lincsApiURL + "place", {
            headers: {
              "Accept": "application/json",
              "Content-Type": "application/json",
              'Authorization': Constants.authToken,
            },
            method: "POST",
            body: JSON.stringify({
              uris: batch,
              language: locale
            }),
          })
            .then((response) => {
              if (response.ok) {
                return response.json();
              }
              throw new Error("FAILURE");
            })
            .then((placeResponse) => {
              for (const place of placeResponse) {
                for (const outgoingPred of response.mainEntityInfo.outgoing) {
                  if (outgoingPred.object == place.uri) {
                    let coords = place.definedBy.replace(
                      "POINT(",
                      "",
                    );
                    coords = coords.replace(")", "");
                    coords = coords.split(" ");
                    coords = [parseFloat(coords[1]), parseFloat(coords[0])];

                    outgoingPred.coords = coords;
                    outgoingPred.zoom = 4;
                  }
                }
              }
              
              resolve(true);
            })
            .catch(() => {
              console.log("Query Error");
              resolve(false);
            });
        }),
      );
    }
  }
  return new Promise((resolve) => {
    Promise.all(promises).then(() => {
      resolve(response);
    });
  });
}

/**
 * Gets group info for main entities
 * @param {*} response 
 * @return a promise of when the group request is complete
 */
function getGroupInfo(response: Entities) {
  return new Promise((resolve) => {
    if (response.mainEntityInfo.type == "http://www.cidoc-crm.org/cidoc-crm/E74_Group") {
      const locale = Globals.locale ? "en" : "fr";
      fetch(Constants.lincsApiURL + "group/statements", {
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          'Authorization': Constants.authToken,
        },
        method: "POST",
        body: JSON.stringify({
          uris: [response.mainEntityInfo.resource],
          language: locale
        }),
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          }
          throw new Error("FAILURE");
        })
        .then((groupResponse) => {
          groupResponse = groupResponse[0].statements;
          groupResponse = groupResponse.filter((statement: any) => {
            let type = statement.type;
            return (type == "type" || type == "member" || type == "work");
          }).map((statement: any) => { 
            return {
              graph: statement.graph,
              predicate: statement.type,
              objectIsURI: true,
              object: statement.uri,
              objectLabel: statement.value
            };
          });
          response.mainEntityInfo.outgoing = response.mainEntityInfo.outgoing.concat(groupResponse);

          resolve(response);
        })
        .catch(() => {
          console.log("Query Error");
          resolve(false);
        });
    } else {
      resolve(null);
    }
  });
}

/**
 * Gets work info for main entities
 * @param {*} response 
 * @return a promise of when the work request is complete
 */
function getWorkInfo(response: Entities) {
  return new Promise((resolve) => {
    if (response.mainEntityInfo.type == "http://iflastandards.info/ns/fr/frbr/frbroo/F1_Work") {
      const locale = Globals.locale ? "en" : "fr";
      fetch(Constants.lincsApiURL + "work/statements", {
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          'Authorization': Constants.authToken,
        },
        method: "POST",
        body: JSON.stringify({
          uris: [response.mainEntityInfo.resource],
          language: locale
        }),
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          }
          throw new Error("FAILURE");
        })
        .then((workResponse) => {
          workResponse = workResponse[0].statements;
          workResponse = workResponse.filter((statement: any) => {
            let type = statement.type;
            return (type == "date" || type == "createPlace" || type == "type" || type == "creator");
          }).map((statement: any) => { 
            if (statement.type == "date") {
              statement.type = "createDate";
            }
            return {
              graph: statement.graph,
              predicate: statement.type,
              objectIsURI: true,
              object: statement.uri,
              objectLabel: statement.value
            };
          });
          response.mainEntityInfo.outgoing = response.mainEntityInfo.outgoing.concat(workResponse);

          resolve(response);
        })
        .catch(() => {
          console.log("Query Error");
          resolve(false);
        });
    } else {
      resolve(null);
    }
  });
}

/**
 * Removes the top entity from the chain
 * @return the top entity is removed from the chain
 */
function popEntity(popFromEntityList = true) {
  // Visually remove from chain
  Globals.d3Data.mainContainer[0].remove();
  Globals.d3Data.bubblesContainer[0].remove();
  Globals.d3Data.highlightLine[0].remove();

  // Update data
  if (popFromEntityList) {
    Globals.entityList.shift();
    Globals.entityApiList.shift();
  }
  Globals.d3Data.mainContainer.shift();
  Globals.d3Data.xMain.shift();
  Globals.d3Data.yMain.shift();
  Globals.d3Data.highlightLine.shift();
  Globals.d3Data.xHighlightLine.shift();
  Globals.d3Data.yHighlightLine.shift();
  Globals.d3Data.xBubbles.shift();
  Globals.d3Data.yBubbles.shift();
  Globals.d3Data.bubblesContainer.shift();
  Globals.d3Data.placementPath.shift();
  Globals.d3Data.placementPathStr.shift();
  Globals.d3Data.flipPlacement.shift();
}