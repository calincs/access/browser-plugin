/* eslint-disable @typescript-eslint/no-explicit-any */

export type Entity = {
  uri: string,
  label: string,
  type?: string,
  page: number,
  predicates: Record<string, Predicate>,
  displayedCategory: string,
}

export type Predicate = {
  maxPage: number,
  predicates: EntityNode[],
}

export type EntityNode = {
  resource?: string,
  resourceLabel?: string,
  annotation?: string,
  annotationLabel?: string,
  type?: string,
  typeLabel?: string,
  objectIsURI?: boolean,
  x?: number,
  y?: number,
}

export type Entities = {
  connectedEntities: any[],
  mainEntityInfo: {
    image: any,
    outgoing: any[],
    resource: string,
    resourceLabel: string,
    type: string,
    typeLabel: string,
    usesDefaultImage: boolean,
  },
}

export const Globals = {
  /** Page Scan Settings */
  // This is a list of all nodes that have been scanned on the page
  allNodes: [] as any[],
  // The number of highlights currently displayed on screen
  numHighlights: 0,
  // This is a list of all words currently highlighted
  highlightedWords: [] as string[],
  // An array holding all highlight elements
  highlightElements: [] as any[],
  // A count of the total number of scan requests
  totalRequests: 0,
  // This is a list of all highlight requests pending response
  inFlightRequests: [] as any[],
  // A count of the total number of completed scan requests
  finishedRequests: 0,
  // An id used to ensure that the scan matches a request response
  scanId: "",

  /** Highlight UI Settings */
  // This is the current list of traversed entities
  entityList: [] as Entity[],
  // This is the current list of entity api responses
  entityApiList: [] as any,
  // This is the actively displayed D3 instance
  activeD3: {} as any,
  // This holds d3 objects that are in use
  d3Data: {} as any,
  // This is the highlight currently focussed on
  focussedHighlight: null as any,
  // This holds the element the graph is currently targeting
  currentTarget: {} as any,
  // This holds the entity info for the entity the graph is displaying
  currentEntity: {} as Entity,
  // This holds the current locale
  locale: true,

  /** Sidebar/Bubble UI Settings */
  // This is the iframe element holding the sidebar
  sidebar: null as any,
  // This is the selection window to minimize/maximize the sidebar
  sidebarSelections: {} as HTMLDivElement,
  // This boolean is set when a graph overlay is displayed
  graphDisplayed: false,
  // A complete copy of the body element that gets placed on extension close
  bodyCopy: "",
  // A boolean for in progress scans
  loading: false,
};
