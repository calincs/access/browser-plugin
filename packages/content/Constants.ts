/** Page Scan Settings */
// This is the size for batching text scans
export const textBatchSize = 3000;
// The amount of time in ms to wait before sending another text segment to scan
export const requestDelay = 10;

/** Side Panel Info Settings */
// The base url of the sidebar iframe
export const sidebarURL = "https://plugin.lincsproject.ca/#/";
// The url for the sidebar's api
export const pluginURL = "https://plugin.lincsproject.ca/";
// The url for accessing the LINCS API
export const lincsApiURL = "https://lincs-api.lincsproject.ca/api/";

/** AuthToken for LincsAPI rate limiting */
export const authToken = "Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJVcG5aOHZaTGNUemhTMWtzdGN2NDVkR205QXYwUmlTbElaQ0YzZEhxR1JRIn0.eyJleHAiOjE3NDIzMjMyMjcsImlhdCI6MTcxMDc4NzIyNywianRpIjoiMjU2OTU1YjUtOTkzNi00OGZmLThjMmMtYjdkOWRiMTliZjc0IiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5saW5jc3Byb2plY3QuY2EvcmVhbG1zL2xpbmNzIiwiYXVkIjpbInZlcnNkLXVpIiwicmVzZWFyY2hzcGFjZSIsImFkbWluLWNsaSIsImJyb2tlciIsImFjY291bnQiXSwic3ViIjoiYmQ5MTRkMTEtZmVkMi00ODllLWI0MGQtNDk0YjkzZDFmOGU4IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoibGluY3MtYXBpIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIvKiJdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsiVXNlciIsImRlZmF1bHQtcm9sZXMtbGluY3MiLCJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsidmVyc2QtdWkiOnsicm9sZXMiOlsidXNlciJdfSwicmVzZWFyY2hzcGFjZSI6eyJyb2xlcyI6WyJjb250cmlidXRvciIsImd1ZXN0Il19LCJhZG1pbi1jbGkiOnsicm9sZXMiOlsiYWRtaW4iXX0sImJyb2tlciI6eyJyb2xlcyI6WyJyZWFkLXRva2VuIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUgYXZhdGFyX3VybCIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiY2xpZW50SG9zdCI6IjcwLjc1LjExMi4xMSIsInByZWZlcnJlZF91c2VybmFtZSI6InNlcnZpY2UtYWNjb3VudC1saW5jcy1hcGkiLCJjbGllbnRBZGRyZXNzIjoiNzAuNzUuMTEyLjExIiwiY2xpZW50X2lkIjoibGluY3MtYXBpIn0.ivjIKG0hWe5ULAseATO4RYUmR8GCEZaTnbE70zoT-xdNxz-jXZcGmTLnGf_t34X3BEX13u-unCPGv7T5M5Wnn6f_ohG5WW8H18oSxiAflSLue3TEpgEODACXS2rzP8gDODof2cC1MSDL9E_YI4lpQ6XAl8aMb-ojhzjZ4RJUbjXq3epQ-rmvg2R5DYtKSPtn8f1o2zooGVZg8lO_Ymi9tAxqr7PsayMXExsD2R_SShrUD9DS-1Vfvrh8a1dxdquUtZ1GHc90Ms08BxdV8hBe0Q8UvQorZgQ6dOUnKUId6250qcENqXjvbttN--FPt4z8BZDphaA38Itm-q0UV4YesQ";

/** Image Settings */
// This const maps entity types to icons
export const iconMapper: Record<string, string> = {
  ACTIVITY: "activity.svg",
  PERSON: "person.svg",
  PLACE: "place.svg",
  GROUP: "group.svg",
  PURSUIT: "pursuit.svg",
  LINGUISTIC_APPELLATION: "linguistic-appellation.svg",
  WORK: "work.svg",
  EXPRESSION: "expression.svg",
  RECORDING: "recording.svg",
  ACQUISITION: "acquisition.svg",
  ATTRIBUTE_ASSIGNMENT: "attribute-assignment.svg",
  BIRTH: "birth.svg",
  FORMATION: "formation.svg",
  MOVE: "move.svg",
  PRODUCTION: "production.svg",
  TRANSFER_OF_CUSTODY: "transfer-of-custody.svg",
  TYPE: "type.svg",
  LINGUISTIC_OBJECT: "linguistic-object.svg",
  CREATION: "creation.svg",
  INFORMATION_OBJECT: "information-object.svg",
  CONCEPTUAL_OBJECT: "conceptual-object.svg",
  CURATED_HOLDING: "curated-holding.svg",
  DEATH: "death.svg",
  DESIGN_OF_PROCEDURE: "design-of-procedure.svg",
  DIGITAL_OBJECT: "digital-object.svg",
  DIMENSION: "dimension.svg",
  EXPRESSION_CREATION: "expression-creation.svg",
  HUMAN_MADE_OBJECT: "human-made-object.svg",
  LANGUAGE: "language.svg",
  MANIFESTATION_STATION: "manifestation-station.svg",
  MATERIAL: "material.svg",
  MEASUREMENT_UNIT: "measurement-unit.svg",
  MONETARY_AMOUNT: "monetary-amount.svg",
  PERFORMANCE: "performance.svg",
  PHYSICAL_OBJECT: "physical-object.svg",
  TIME_SPAN: "time-span.svg",
  TRANSCRIPTION: "transcription.svg",
  VISUAL_ITEM: "visual-item.svg",
  PROPORTIONAL_OBJECT: "proportional-object.svg",
  IDENTIFIER: "identifier.svg",
};

/** UI Constants */
// HTML block for inserting selections to the left of the sidebar
export const sidebarSelectionsHtml = `
<div class="node" style="display: flex; flex-direction: column; justify-content: center; align-items: center;">
    <div class="node" id="lincs-toggle-sidebar" style="width: 50px; height: 50px; cursor: pointer; justify-content: center; align-items: center; display: flex; border: 0px solid grey;" tabindex="0">
        <svg class="node" id="lincs-minimize" xmlns="http://www.w3.org/2000/svg" width="15" height="30" viewBox="0 0 9 14" fill="none">
            <path class="node" d="M5.672 7.00001L0.722 11.95L2.136 13.364L8.5 7.00001L2.136 0.636013L0.722 2.05001L5.672 7.00001Z" fill="#525455" stroke-width="1"/>
        </svg>
        <svg class="node" id="lincs-maximize" style="display: none;" xmlns="http://www.w3.org/2000/svg" width="15" height="30" viewBox="0 0 9 14" fill="none">
            <path class="node" d="M3.32802 7.00001L8.27802 11.95L6.86402 13.364L0.500015 7.00001L6.86402 0.636013L8.27802 2.05001L3.32802 7.00001Z" fill="#525455" stroke-width="1"/>
        </svg>
    </div>
    <div class="node" id="lincs-toggle-vis" style="width: 50px; height: 50px; cursor: pointer; justify-content: center; align-items: center; display: flex; border: 0px solid grey;" tabindex="0">
        <svg class="node" id="lincs-graph-shown" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 17 16" fill="none">
            <g class="node" clip-path="url(#clip0_9_112)">
                <path class="node" d="M7.1 2.82664C7.55889 2.71923 8.02871 2.66554 8.5 2.66664C13.1667 2.66664 15.8333 7.99998 15.8333 7.99998C15.4287 8.75705 14.946 9.4698 14.3933 10.1266M9.91334 9.41331C9.73024 9.60981 9.50944 9.76741 9.26411 9.87673C9.01877 9.98604 8.75394 10.0448 8.4854 10.0496C8.21686 10.0543 7.95011 10.0049 7.70108 9.9043C7.45204 9.80371 7.22582 9.654 7.0359 9.46408C6.84599 9.27416 6.69627 9.04794 6.59568 8.7989C6.49509 8.54987 6.44569 8.28312 6.45043 8.01458C6.45517 7.74604 6.51394 7.48121 6.62326 7.23587C6.73257 6.99054 6.89017 6.76974 7.08667 6.58664M12.46 11.96C11.3204 12.8286 9.93274 13.3099 8.5 13.3333C3.83334 13.3333 1.16667 7.99998 1.16667 7.99998C1.99593 6.45457 3.1461 5.10438 4.54 4.03998L12.46 11.96Z" stroke="#525455" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                <path class="node" d="M1.16667 0.666687L15.8333 15.3334" stroke="#525455" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
            </g>
            <defs class="node">
                <clipPath class="node" id="clip0_9_112">
                    <rect class="node" width="16" height="16" fill="white" transform="translate(0.5)"/>
                </clipPath>
            </defs>
        </svg>
        <svg class="node" id="lincs-graph-hidden" style="display: none;" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 17 16" fill="none">
            <g class="node" clip-path="url(#clip0_17_76)">
                <g class="node" clip-path="url(#clip1_17_76)">
                    <path class="node" d="M1.16669 8.00002C1.16669 8.00002 3.83335 2.66669 8.50002 2.66669C13.1667 2.66669 15.8334 8.00002 15.8334 8.00002C15.8334 8.00002 13.1667 13.3334 8.50002 13.3334C3.83335 13.3334 1.16669 8.00002 1.16669 8.00002Z" stroke="#083943" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    <path class="node" d="M8.5 10C9.60457 10 10.5 9.10457 10.5 8C10.5 6.89543 9.60457 6 8.5 6C7.39543 6 6.5 6.89543 6.5 8C6.5 9.10457 7.39543 10 8.5 10Z" stroke="#083943" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </g>
            </g>
            <defs class="node">
                <clipPath class="node" id="clip0_17_76">
                    <rect class="node" width="16" height="16" fill="white" transform="translate(0.5)"/>
                </clipPath>
                <clipPath class="node" id="clip1_17_76">
                    <rect class="node" width="16" height="16" fill="white" transform="translate(0.5)"/>
                </clipPath>
            </defs>
        </svg>
    </div>
</div
`;
// CSS block for inserting selections to the left of the sidebar
export const sidebarSelectionsCss = "height: 100px; width: 50px; position: fixed; top: 0; right: 350px; z-index: 2147483647; background-color: white; box-shadow: -2px 0px 12px rgba(22, 22, 22, 0.24)";
// CSS block for the sidebar placement
export const sidebarCss = "width: 350px; height: 100vh; position: fixed; top: 0; right: 0; z-index: 2147483647; background-color: white; border: none; border-left: 0px solid #083943; user-select: none; box-shadow: -2px 0px 12px rgba(22, 22, 22, 0.24)";