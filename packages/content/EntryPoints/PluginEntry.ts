import * as UIController from "@ce-content/UIController";
import * as PageScanService from "@ce-content/PageScanService";
import * as DataService from "@ce-content/DataService";
import * as BubblesDisplayService from "@ce-content/BubblesDisplayService";
import { Globals } from "@ce-content/Globals";

/* Message listeners and dispatchers */
chrome.runtime.onMessage.addListener((request, sender, reply) => {
  if (request.type == "full-page-scan") PageScanService.fullScanAndHighlight(reply);
  if (request.type == "highlighted-text-scan") PageScanService.textScanAndHighlight(reply);
  if (request.type == "get-title") reply(document.title);
  if (request.type == "check-highlight-scan") PageScanService.checkHighlightScan(reply);
  return true;
});

/* Sidebar message listeners and dispatchers */
window.addEventListener("message", function (event) {
  if (Globals.sidebar != null) { 
    if (event.data.type == "get-filters") PageScanService.getFiltersToIframe();
    if (event.data.type == "get-settings") PageScanService.getSettingsToIframe();
    if (event.data.type == "show-entity-select") UIController.showEntitySelect();
    if (event.data.type == "show-entity-graph") DataService.showEntityGraph(event.data, true);
    if (event.data.type == "show-category-entities") DataService.drawCategoryEntities(event.data.category);
    if (event.data.type == "entity-traversal-back") DataService.entityTraversalBack(event.data.amount);
    if (event.data.type == "stop-scan") PageScanService.stopScan();
    if (event.data.type == "exit") UIController.exitExtension();
    if (event.data.type == "scroll-to-highlight") UIController.scrollToHighlight(event.data.highlightNum, event.data.selectHighlightImmediatly);
    if (event.data.type == "add-entity-to-graph") DataService.addNewFocus({ type: "show-entity-graph", label: event.data.label, uri: event.data.uri, page: 1, predicates: {}, displayedCategory: "" });
    if (event.data.type == "remove-and-add-entity-to-graph") DataService.removeAndAddNewFocus({ type: "show-entity-graph", label: event.data.label, uri: event.data.uri, page: 1, predicates: {}, displayedCategory: "" });
    if (event.data.type == "return-to-annotations") UIController.returnToAnnotations();
    if (event.data.type == "display-annotation-info") UIController.displayAnnotationInfo(event.data.annotation);
    if (event.data.type == "exit-graph-display") BubblesDisplayService.exitGraphDisplay();
    if (event.data.type == "expand-sidebar-width") UIController.expandSidebarWidth();
    if (event.data.type == "contract-sidebar-width") UIController.contractSidebarWidth();
  }
});