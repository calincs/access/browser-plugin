/**
 * Get the settings object
 * @returns a promise for the returned settings
 */
function getSettings() {
  return new Promise((resolve) => {
    chrome.runtime.sendMessage({ type: 'get-settings' }, (response) => {
      resolve(response);
    });
  });
}

/**
 * Get the filters object
 * @returns a promise for the returned filters
 */
function getFilters() {
  return new Promise((resolve) => {
    chrome.runtime.sendMessage({ type: 'get-filters' }, (response) => {
      resolve(response);
    });
  });
}

/**
 * Send a message and wait for response
 * @param message The message being sent
 * @returns A promise for the returned data
 */
function sendMessage(message: any) {
  return new Promise((resolve) => {
    chrome.runtime.sendMessage(message, (response) => {
      resolve(response);
    });
  });
}

export { getSettings, getFilters, sendMessage };