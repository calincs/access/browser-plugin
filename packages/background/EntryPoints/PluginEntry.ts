import * as PageScanService from "@ce-background/PageScanService";
import * as SidebarInfoService from "@ce-background/SidebarInfoService";
import * as ConfigService from "@ce-background/ConfigService";

/* Message listener and dispatcher */
chrome.runtime.onMessage.addListener((request, sender, reply) => {
  if (request.type == "start-scan") PageScanService.startScan(request, reply);
  if (request.type == "scan-URIs") PageScanService.scanURIs(request, reply);
  if (request.type == "get-entity-info") SidebarInfoService.getEntityInfo(request, reply);
  if (request.type == "get-related-entities") SidebarInfoService.getRelatedEntities(request, reply);
  if (request.type == "get-filters") ConfigService.getFilters(reply);
  if (request.type == "set-filters") ConfigService.setFilters(request, reply);
  if (request.type == "get-settings") ConfigService.getSettings(reply);
  if (request.type == "set-settings") ConfigService.setSettings(request, reply);

  return true;
});