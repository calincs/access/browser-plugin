import * as PageScanService from "@ce-background/PageScanService";
import * as SidebarInfoService from "@ce-background/SidebarInfoService";
import * as ConfigService from "@ce-background/ConfigService";

/* Message listener and dispatcher */
addEventListener("message", (request: any) => {
  const channel = new BroadcastChannel(request.data.responseChannel);
  function reply(response: any) {
    response.type = request.data.type;
    channel.postMessage(response);
  }

  if (request.data.type == "start-scan") PageScanService.startScan(request.data, reply);
  if (request.data.type == "scan-URIs") PageScanService.scanURIs(request.data, reply);
  if (request.data.type == "get-entity-info") SidebarInfoService.getEntityInfo(request.data, reply);
  if (request.data.type == "get-related-entities") SidebarInfoService.getRelatedEntities(request.data, reply);
  if (request.data.type == "get-filters") ConfigService.getFilters(reply);
  if (request.data.type == "set-filters") ConfigService.setFilters(request.data, reply);
  if (request.data.type == "get-settings") ConfigService.getSettings(reply);
  if (request.data.type == "set-settings") ConfigService.setSettings(request.data, reply);
});