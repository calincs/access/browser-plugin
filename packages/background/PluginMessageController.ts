/**
 * Get the settings object
 * @returns a promise for the returned settings
 */
function getSettings(request: any) {
  return new Promise((resolve) => {
    chrome.storage.sync.get("active-settings", (settings) => {
      resolve(settings);
    });
  });
}
  
/**
 * Get the filters object
 * @returns a promise for the returned filters
 */
function getFilters(request: any) {
  return new Promise((resolve) => {
    chrome.storage.sync.get("active-filters", (filters) => {
      resolve(filters);
    });
  });
}

export { getSettings, getFilters };