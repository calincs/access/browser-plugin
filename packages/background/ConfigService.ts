import * as Constants from "@ce-background/Constants";

/**
 * Gets all default and active filters
 * @param {*} reply the reply callback for the requesting message
 * @return An object containing default and active filters
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function getFilters(reply: (response?: any) => void) {
  chrome.storage.sync.get("active-filters", (filters) => {
    reply({
      allFilters: JSON.parse(JSON.stringify(Constants.allFilters)),
      activeFilters: filters["active-filters"] == null ? JSON.parse(JSON.stringify(Constants.allFilters)) : filters["active-filters"]
    });
  });
}

/**
 * Sets active filters by replacement
 * @param {*} request the incoming request from the message listener containing the new filters
 * @param {*} reply the reply callback for the requesting message
 * @return A success acknowledgement
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function setFilters(request: any, reply: (response?: any) => void) {
  chrome.storage.sync.set({ "active-filters": request.filters }, () => {
    reply({ type: "success" });
  });
}

/**
 * Gets all default and active settings
 * @param {*} reply the reply callback for the requesting message
 * @return An object containing default and active settings
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function getSettings(reply: (response?: any) => void) {
  chrome.storage.sync.get("active-settings", (settings) => {
    const defaultSettings = settings["active-settings"] == null;

    reply({
      allSettings: JSON.parse(JSON.stringify(Constants.allSettings)),
      activeSettings: defaultSettings ? JSON.parse(JSON.stringify(Constants.allSettings)) : settings["active-settings"],
      defaultSettings: defaultSettings
    });
  });
}

/**
 * Sets active settings by replacement
 * @param {*} request the incoming request from the message listener containing the new settings
 * @param {*} reply the reply callback for the requesting message
 * @return A success acknowledgement
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function setSettings(request: any, reply: (response?: any) => void) {
  chrome.storage.sync.set({ "active-settings": request.settings }, () => {
    reply({ type: "success" });
  });
}