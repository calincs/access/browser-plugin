/**
 * Get the settings object
 * @returns a promise for the returned settings
 */
function getSettings(request: any) {
  return new Promise((resolve) => {
    resolve({"active-settings": request.settings});
  });
}
  
/**
 * Get the filters object
 * @returns a promise for the returned filters
 */
function getFilters(request: any) {
  return new Promise((resolve) => {
    resolve({"active-filters": request.filters});
  });
}

export { getSettings, getFilters };