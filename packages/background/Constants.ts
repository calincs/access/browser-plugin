/** Page Scan Settings */
// This is the size for batching triplestore queries
export const queryChunkSize = 10;
// This is the url for getting a list of candidate matches on a text
export const reconciliationURL = "https://authority.lincsproject.ca/reconcile/any";
// This is the url for getting a list of inline uris
export const uriExistsURL = "https://lincs-api.lincsproject.ca/api/entity/exists";

/** SpaCy Settings */
// This is the url for spaCy NER lookup
export const spaCyURL = "https://lincs-api.lincsproject.ca/api/ner";

/** Side Panel Info Settings */
// This is the url for getting all triples associated with an entity
export const entityInfoURL = "https://lincs-api.lincsproject.ca/api/entity";
// This is the url for getting a list of relationships on a subject
export const aggregationURL = "https://lincs-api.lincsproject.ca/api/linked";
// This is the url for getting a list of relationships between two subjects
export const doubleAggregationURL = "https://lincs-api.lincsproject.ca/api/links";

/** AuthToken for LincsAPI rate limiting */
export const authToken = "Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJVcG5aOHZaTGNUemhTMWtzdGN2NDVkR205QXYwUmlTbElaQ0YzZEhxR1JRIn0.eyJleHAiOjE3NDIzMjMyMjcsImlhdCI6MTcxMDc4NzIyNywianRpIjoiMjU2OTU1YjUtOTkzNi00OGZmLThjMmMtYjdkOWRiMTliZjc0IiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5saW5jc3Byb2plY3QuY2EvcmVhbG1zL2xpbmNzIiwiYXVkIjpbInZlcnNkLXVpIiwicmVzZWFyY2hzcGFjZSIsImFkbWluLWNsaSIsImJyb2tlciIsImFjY291bnQiXSwic3ViIjoiYmQ5MTRkMTEtZmVkMi00ODllLWI0MGQtNDk0YjkzZDFmOGU4IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoibGluY3MtYXBpIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIvKiJdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsiVXNlciIsImRlZmF1bHQtcm9sZXMtbGluY3MiLCJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsidmVyc2QtdWkiOnsicm9sZXMiOlsidXNlciJdfSwicmVzZWFyY2hzcGFjZSI6eyJyb2xlcyI6WyJjb250cmlidXRvciIsImd1ZXN0Il19LCJhZG1pbi1jbGkiOnsicm9sZXMiOlsiYWRtaW4iXX0sImJyb2tlciI6eyJyb2xlcyI6WyJyZWFkLXRva2VuIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUgYXZhdGFyX3VybCIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiY2xpZW50SG9zdCI6IjcwLjc1LjExMi4xMSIsInByZWZlcnJlZF91c2VybmFtZSI6InNlcnZpY2UtYWNjb3VudC1saW5jcy1hcGkiLCJjbGllbnRBZGRyZXNzIjoiNzAuNzUuMTEyLjExIiwiY2xpZW50X2lkIjoibGluY3MtYXBpIn0.ivjIKG0hWe5ULAseATO4RYUmR8GCEZaTnbE70zoT-xdNxz-jXZcGmTLnGf_t34X3BEX13u-unCPGv7T5M5Wnn6f_ohG5WW8H18oSxiAflSLue3TEpgEODACXS2rzP8gDODof2cC1MSDL9E_YI4lpQ6XAl8aMb-ojhzjZ4RJUbjXq3epQ-rmvg2R5DYtKSPtn8f1o2zooGVZg8lO_Ymi9tAxqr7PsayMXExsD2R_SShrUD9DS-1Vfvrh8a1dxdquUtZ1GHc90Ms08BxdV8hBe0Q8UvQorZgQ6dOUnKUId6250qcENqXjvbttN--FPt4z8BZDphaA38Itm-q0UV4YesQ";

/** Extension Configuration Settings */
// This is a list of available default filters

export type FilterItem = {
  label: string,
  value: string,
  active: boolean,
};

export type Filters = {
  ner: {
    label: string,
    filters: FilterItem[],
  },
  type: {
    label: string,
    filters: FilterItem[],
  },
  graph: {
    label: string,
    filters: FilterItem[],
  },
};

export const allFilters: Filters = {
  ner: {
    label: "ner_categories",
    filters: [
      { label: "named_events", value: "EVENT", active: true },
      { label: "facilities", value: "FAC", active: true },
      { label: "countries", value: "GPE", active: true },
      { label: "named_docs", value: "LAW", active: true },
      { label: "non_state", value: "LOC", active: true },
      { label: "nationalities", value: "NORP", active: true },
      { label: "orgs", value: "ORG", active: true },
      { label: "people", value: "PERSON", active: true },
      { label: "objects", value: "PRODUCT", active: true },
      { label: "works", value: "WORK_OF_ART", active: true }
    ] as FilterItem[],
  },
  type: {
    label: "entity_types",
    filters: [
      { label: "work", value: "http://iflastandards.info/ns/fr/frbr/frbroo/F1_Work", active: true },
      { label: "expression", value: "http://iflastandards.info/ns/fr/frbr/frbroo/F2_Expression", active: true },
      { label: "serial_work", value: "http://iflastandards.info/ns/fr/frbr/frbroo/F18_Serial_Work", active: true },
      { label: "man_made", value: "http://www.cidoc-crm.org/cidoc-crm/E22_Man-Made_Object", active: true },
      { label: "image", value: "http://www.cidoc-crm.org/cidoc-crm/E38_Image", active: true },
      { label: "visual", value: "http://www.cidoc-crm.org/cidoc-crm/E36_Visual_Item", active: true },
      { label: "digital", value: "http://www.ics.forth.gr/isl/CRMdig/D1_Digital_Object", active: true },
      { label: "actor", value: "http://www.cidoc-crm.org/cidoc-crm/E39_Actor", active: true },
      { label: "person", value: "http://www.cidoc-crm.org/cidoc-crm/E21_Person", active: true },
      { label: "group", value: "http://www.cidoc-crm.org/cidoc-crm/E74_Group", active: true },
      { label: "place", value: "http://www.cidoc-crm.org/cidoc-crm/E53_Place", active: true }
    ] as FilterItem[],
  },
  graph: {
    label: "datasets",
    filters: [
      { label: "adarchive", value: "http://graph.lincsproject.ca/adarchive", active: true },
      { label: "antho_graeca", value: "http://graph.lincsproject.ca/anthologiaGraeca", active: true },
      { label: "can_hist", value: "http://graph.lincsproject.ca/hist-canada/hist-cdns", active: true },
      { label: "ethno", value: "http://graph.lincsproject.ca/ethnomusicology", active: true },
      { label: "histsex", value: "http://graph.lincsproject.ca/histsex", active: true },
      { label: "ind_aff", value: "http://graph.lincsproject.ca/hist-canada/ind-affairs", active: true },
      { label: "moeml", value: "http://graph.lincsproject.ca/moeml", active: true },
      { label: "orlando", value: "http://graph.lincsproject.ca/orlando", active: true },
      { label: "usaskart", value: "http://graph.lincsproject.ca/usaskart", active: true },
      { label: "y90s", value: "http://graph.lincsproject.ca/yellowNineties", active: true },
    ] as FilterItem[],
  },
};
// This is a list of all available default settings
export const allSettings = {
  hidden: {
    label: "hidden",
    settings: {
      languageSelection: {
        value: true
      }
    }
  },
  highlight: {
    label: "highlight_settings",
    settings: {
      displayFirstHighlight: {
        type: "switch",
        label: "display_first",
        value: false
      },
      highlightRelevanceScore: {
        type: "range",
        label: "highlight_relevance",
        value: 10
      }
    }
  }
}