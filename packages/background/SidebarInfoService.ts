import * as Constants from "@ce-background/Constants";

// The message controller is swapped depending on the build
let MessageController: any = null;
import(
  process.env.BUILD_ENV == 'PLUGIN' ? 
  "@/packages/background/PluginMessageController" : 
  "@/packages/background/LibraryMessageController"
).then((controller) => {MessageController = controller});

/**
 * Gets a list of relationships for a given entity
 * @param {*} request the incoming request from the message listener containing the uri for lookup
 * @param {*} reply the reply callback for the requesting message
 * @return An array of objects that contain triple information
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function getEntityInfo(request: any, reply: (response?: any) => void) {
  MessageController.getSettings(request).then((settings: any) => {
    const activeSettings = settings["active-settings"] == null ? JSON.parse(JSON.stringify(Constants.allSettings)) : settings["active-settings"];
    const locale = activeSettings.hidden.settings.languageSelection.value ? "en" : "fr";

    fetch(Constants.entityInfoURL, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': Constants.authToken,
      },
      method: "POST",
      body: JSON.stringify({ uris: [request.uri], language: locale })
    }).then((response1) => {
      if (response1.status == 200) {
        return response1.json();
      }
      throw new Error("FAILURE");
    }).then((response1) => {
      response1 = response1[0];

      fetch(Constants.aggregationURL, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': Constants.authToken,
        },
        method: "POST",
        body: JSON.stringify({ uri: request.uri, language: locale })
      }).then((response2) => {
        if (response2.status == 200) {
          return response2.json();
        } else {
          reply({ connectedEntities: [], mainEntityInfo: response1 });
        }
        throw new Error("FAILURE");
      }).then((response2) => {
        reply({ connectedEntities: response2, mainEntityInfo: response1 });
      }).catch(() => {});
    }).catch(() => {
      reply([]);
    });
  });
}

/**
 * Gets a list of relationships between two entities
 * @param {*} request the incoming request from the message listener containing the two uris for lookup
 * @param {*} reply the reply callback for the requesting message
 * @return An array of objects that contain triple information
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function getRelatedEntities(request: any, reply: (response?: any) => void) {
  MessageController.getSettings(request).then((settings: any) => {
    const activeSettings = settings["active-settings"] == null ? JSON.parse(JSON.stringify(Constants.allSettings)) : settings["active-settings"];
    const locale = activeSettings.hidden.settings.languageSelection.value ? "en" : "fr";

    const query = JSON.stringify({
      uri: request.firstURI,
      secondUri: request.secondURI,
      language: locale,
      predicateFilter: [
        "http://www.cidoc-crm.org/cidoc-crm/P107_has_current_or_former_member",
        "http://www.cidoc-crm.org/cidoc-crm/P67_refers_to"
      ]
    });
    fetch(Constants.doubleAggregationURL, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': Constants.authToken,
      },
      method: "POST",
      body: query
    }).then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error("FAILURE");
    }).then((response) => {
      reply(response);
    }).catch(() => {
      reply([]);
    });
  });
}