import * as Constants from "@ce-background/Constants";

// The message controller is swapped depending on the build
let MessageController: any = null;
import(
  process.env.BUILD_ENV == 'PLUGIN' ? 
  "@/packages/background/PluginMessageController" : 
  "@/packages/background/LibraryMessageController"
).then((controller) => {MessageController = controller});

type NerEntity = {
  name: string,
  label: string,
};
type Property = {
  pid: string,
  v: string,
};
type QueryObject = {
  [key: string]: {
    query: string,
    type: string[],
    limit: number,
    properties: Property[],
  };
};

/**
 * Takes text from the page as input, runs NER lookup and Queries triplestore for matches
 * @param {*} request the incoming request from the message listener containing the text to scan
 * @param {*} reply the reply callback for the requesting message
 * @return An array of words that have been matched to entities, organized from longest to shortest word length
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function startScan(request: any, reply: (response?: any) => void) {
  nerLookup(request).then((entities) => {
    MessageController.getSettings(request).then((settings: any) => {
      MessageController.getFilters(request).then((filters: any) => {
        const activeSettings = settings["active-settings"] == null ? Constants.allSettings : settings["active-settings"];
        const activeFilters: Constants.Filters = filters["active-filters"] == null ? Constants.allFilters : filters["active-filters"];
        activeFilters.ner.filters = activeFilters.ner.filters.filter((filter: Constants.FilterItem) => filter.active);
        activeFilters.type.filters = activeFilters.type.filters.filter((filter: Constants.FilterItem) => filter.active);
        activeFilters.graph.filters = activeFilters.graph.filters.filter((filter: Constants.FilterItem) => filter.active);
        getCandidateMatches(entities, activeFilters, Number(activeSettings.highlight.settings.highlightRelevanceScore.value) + 80)
          .then((words) => {
            reply(words);
          }).catch((response) => {
            reply(["LINCS-ERROR", response]);
          });
      });
    })
  }).catch((response) => {
    reply(["LINCS-ERROR", response]);
  });
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function scanURIs(request: any, reply: (response?: any) => void) {
  const uriList: string = JSON.stringify(request.uris);
  fetch(Constants.uriExistsURL, {
    method: "POST",
    headers: {
      'Content-Type': 'application/json',
      'Authorization': Constants.authToken,
    },
    body: uriList,
  }).then((response) => {
    if (response.ok) {
      return response.json();
    }
    reply(["LINCS-ERROR", response]);
  }).then((response) => {
    reply(response);
  }).catch((response) => {
    reply(["LINCS-ERROR", response]);
  });
}

/**
 * Takes a string and runs NER lookup using SpaCy
 * @param {*} text the string to search
 * @return An array of NER entities that the NER lookup has found
 */
function nerLookup(request: any): Promise<NerEntity[]> {
  return new Promise((resolve, reject) => {
    MessageController.getSettings(request).then((settings: any) => {
      const activeSettings = settings["active-settings"] == null ?
        Constants.allSettings : settings["active-settings"];
      const locale = activeSettings.hidden.settings.languageSelection.value ? "en" : "fr";
      fetch(Constants.spaCyURL, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': Constants.authToken,
        },
        method: "POST",
        body: JSON.stringify({
          text: request.text,
          language: locale
        })
      }).then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          reject({
            status: response.status,
            statusText: response.statusText
          });
        }
      }).then((response) => {
        if (response != null) {
          resolve(response.entities as NerEntity[]);
        }
      });
    });
  });
}

/**
 * Takes an array of words/phrases and gets candidate entity matches
 * @param {*} entities an array of NER entities
 * @return An array of words/phrases that have been matched to entities in the triplestore
 */
function getCandidateMatches(entities: NerEntity[], filters: Constants.Filters, relevancyScore: number): Promise<string[]> {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const promises: Promise<any>[] = [];
  for (let i = 0; i < entities.length; i += Constants.queryChunkSize) {
    const chunk = entities.slice(i, i + Constants.queryChunkSize);
    const query = createReconciliationQuery(chunk, filters);
    promises.push(new Promise((resolve) => {
      fetch(Constants.reconciliationURL, {
        method: "POST",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
          'Authorization': Constants.authToken,
        },
        body: `queries=${encodeURIComponent(JSON.stringify(query))}`,
      }).then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          resolve({
            status: response.status,
            statusText: response.statusText
          });
        }
      }).then((response) => {
        resolve([query, response]);
      }).catch(() => {
        resolve([
          query,
          { status: "PARSE-FAILURE" }
        ]);
      });
    }));
  }

  return Promise.all(promises).then((responses) => {
    const foundEntities = processFoundEntities(responses, relevancyScore);
    return foundEntities;
  });
}

/** Helper Functions */

/**
 * Create a query for the reconciliationURL
 * @param {*} chunk a collection of entities to query for in a batch 
 * @param {*} filters the active filters to apply to the query
 * @returns a query for the reconciliationURL endpoint
 */
function createReconciliationQuery(chunk: NerEntity[], filters: Constants.Filters) {

  const properties = [] as Property[];
  for (const graph of filters.graph.filters) {
    properties.push({
      pid: "http://graph.lincsproject.ca/",
      v: graph.value,
    });
  }
  const validLabels = new Set(filters.ner.filters.map(filter => filter.value));
  const filteredEntities = chunk.filter(entity => validLabels.has(entity.label));
  const query: QueryObject = {};
  let count = 1;
  for (const entity of filteredEntities) {
    query['q' + count] = {
      query: entity.name
        .replaceAll("/", "//")
        .replace(/([\\])/g, '\\\\$1')
        .replace(/[:"[\]()^\n]/g, ' ')
        .trim(),
      type: filters.type.filters.map((filter: Constants.FilterItem) => filter.value),
      limit: 1,
      properties: properties
    }
    count += 1;
  }

  return query;
}

/**
 * Process the result of the reconciliationURL endpoint
 * @param {*} responses a list of all responses from reconciliation batches
 * @param {*} relevancyScore the relevancy score to apply on matches
 * @returns A list of words that have been found as scanned matches
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function processFoundEntities(responses: any, relevancyScore: number): string[] {
  const foundEntities: string[] = [];
  for (const response of responses) {
    if (response[1].status == null) {
      for (let i = 1; i <= Constants.queryChunkSize; i++) {
        if (response[0]['q' + i] == null) {
          break;
        } else if (response[1]['q' + i].result[0] != null && response[1]['q' + i].result[0].score > relevancyScore) {
          foundEntities.push(response[0]['q' + i].query);
        }
      }
    }
  }

  foundEntities.sort(function (a, b) {
    return b.length - a.length;
  });

  return foundEntities;
}