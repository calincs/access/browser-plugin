# LINCS Context Explorer

This project contains the components to create a browser plugin that adds a panel on the page that is similar to the knowledge panel on the right of the page when you do a Google search. In addition, the plugin also draws a bubble graph in the context of the page when a user explores the data.

This repository is configured as a monorepo. There is an `apps` folder that contains a node server, the plugin popup (Vue) that configures the plugin, and a Vue sidebar that contains the knowledge panel. The `packages` folder contains the background and content scripts that drives the data and drawing of the d3.js display respectively. Source code is written in TypeScript and compiled and packaged with *Tsup*.

## Installing the plugin

1. Pull the repo into a local folder on your computer.
1. Change into the repo folder and build the extension via `npm run build`. Node 20+ is required.
1. Open a Chromium based browser and navigate to the extensions page through the menu (or by navigating to `chrome://extensions` or `edge://extensions` for Chrome and Edge respectively).
1. Turn on developer mode (top-right corner for Chrome, bottom-left for Edge).
1. Select the `Load Unpacked` button and provide the folder's filepath so that the root of the selected folder contains `manifest.json` (Should be `repository_root/plugin`).
1. The extension will load and be enabled. You can pin the extension in your browser bar. Click on the LINCS icon to activate the extension on the current page.

## Installing the library

1. Download the js files located in the [library folder](https://gitlab.com/calincs/access/browser-plugin/-/tree/main/library) and serve them from your site's domain. To prevent XSS attacks, service workers must be served from the same domain as the host site.
2. Add the script tags to your site to load the Context Explorer Library:
```
<script src="/context-explorer.js"></script>
<script src="/d3.min.js"></script>
<script src="/leaflet.js"></script>
```
3. Add [optional/required](https://gitlab.com/calincs/access/browser-plugin/-/blob/main/README_library.md) parameters to the window object
```
<script>
    window.lincs_params = {}
</script>
```
4. Trigger the extension via
```
<script>
    window.launchContextExplorer();
</script>
```

## Technical specification

- The extension should work for all Chromium based browsers. It is built using the Chrome extension development kit and manifest definition.
- This project uses the `LINCS Authority` service to perform a text index search of the LINCS Fuseki triplestore. This search is used to score and return candidate entities for display.
- This project then uses the `LINCS-API` service to query for specific information related to entities that exist in the LINCS Fuseki triplestore
- This project also queries Wikidata as an additional source of data to enrich the display to the user

## Development

### Chromium Extension (Scanning, Highlights, and Node/Edge Visualization)

The chromium extension is configured via `./plugin/manifest.json`. Any modifications to the deployment or setup of the extension is controlled by that file. For modifying the codebase, use the modular files located in `./packages`. These files are compiled together and minimized by Tsup.

### Vue frontend (Sidebar)

The vue frontend is contained within `./apps/sidebar`. There are multiple messages communicated back and forth between the content script and the iframe where the sidebar is loaded into via postMessage and url params.

### Node.JS backend (Extra Data Querying)

The Node.js backend is contained within `./apps/node`. This is responsible for making queries to Wikidata and serving the files of the Vue sidebar. The server is packaged in the Dockerfile image and deployed locally with the Docker Compose file located in the root of the repo. Deployment to LINCS infrastructure is configured with the `.gitlab-ci.yml` file.

## Local Testing

Required for testing sidebar changes locally before deployment. Since the local Vue server hosts in http, https sites are unable to load due to encrypted vs non-encrypted communication.

### Vue Frontend Testing

1. Launch the vue dev server via `cd apps/sidebar && npm run lint && npm run serve`.
2. Modify the `sidebarURL` variable in `./plugin-src/content/Constants.js` to match the url vue is serving to (usually `http://localhost:8080/#/`).
3. Build the extension via `npm run dev`.
4. Reload the extension in chrome by navigating to `chrome://extensions` in the URL bar of chrome and pressing the refresh button for the LINCS extension.
5. Launch the http test site via `cd test-site && python3 -m http.server 8000`.
6. Navigate to `http://localhost:8000` in Chrome and launch the extension as normal. The extension should now be running off your local server. The vue dev server is live, so any changes made to the code will automatically update and display in the sidebar upon saving the modified file.

### Node.JS Backend Testing

1. Launch the node dev server via `docker compose up --build`.
2. Modify the `pluginURL` variable in `./plugin-src/content/Constants.js` to match the url node is serving to (usually `http://localhost:8080`).
3. Build the extension via `npm run dev`.
4. Reload the extension in chrome by navigating to `chrome://extensions` in the URL bar of chrome and pressing the refresh button for the LINCS extension.
5. Launch the http test site via `cd test-site && python3 -m http.server 8000`.
6. Navigate to `http://localhost:8000` in Chrome and launch the extension as normal. The extension should now be running off your local server.
