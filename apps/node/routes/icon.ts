import express, { Request, Response } from 'express';
import { parseFavicon } from "parse-favicon";

const router = express.Router();

router.get('/', (req, res) => {
  handleGetRequest(req, res);
});

function handleGetRequest(req: Request, res: Response) {
  const pageUrl = req.query.target?.toString() ?? "";

  try {
    parseFavicon(pageUrl, textFetcher, bufferFetcher)
      .subscribe((icon) => {
        res.status(200);
        res.send(icon);
      });
  } catch (error) { errorHandle(error); }

  async function textFetcher(url: string): Promise<string> {
    try {
      const res = await fetch(resolveURL(url, pageUrl));
      if (!res.ok) {
        throw new Error(`Network response was not ok: ${res.status} ${res.statusText}`);
      }
      return await res.text();
    } catch (error) {
      errorHandle(error);
    }
    return "";
  }

  async function bufferFetcher(url: string): Promise<ArrayBuffer> {
    try {
      const res = await fetch(resolveURL(url, pageUrl));
      if (!res.ok) {
        throw new Error(`Network response was not ok: ${res.status} ${res.statusText}`);
      }
      return await res.arrayBuffer();
    } catch (error) {
      errorHandle(error);
    }
    return new ArrayBuffer(0);
  }

  function resolveURL(url: string, base: string): string {
    let resolvedURL = "";
    try {
      const rURL = new URL(url, base);
      resolvedURL = rURL.href;
    } catch (error) { errorHandle(error); }

    return resolvedURL;
  }

  function errorHandle(error: any) {
    console.error(error);
    res.status(404);
    res.send();
  }
}

export default router;
