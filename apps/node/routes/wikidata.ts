import express, { Request, Response } from 'express';
import axios from 'axios';

const wikidataEndpoint = "https://query.wikidata.org/bigdata/namespace/wdq/sparql";

const router = express.Router();

router.get('/', (req, res) => {
  handleGetRequest(req, res);
});

function handleGetRequest(req: Request, res: Response) {
  let promises: any = [];
  let i = 0;
  let uris = JSON.parse(req.query.uris!.toString());
  for (const uri of uris) {
    promises.push(new Promise((resolve) => {
      setTimeout(() => {
        try {
          requestData(uri).then((response) => resolve(response));
        } catch (e) {
          resolve(null);
        }
      }, 500 * i);
    }));
    i += 1;
  }

  Promise.all(promises).then((responses) => {
    let output = [];
    for (const response of responses) {
      if (response && response.data.results.bindings.length > 0) {
        output.push(response.data.results.bindings[0]);
      }
    }

    res.status(200);
    res.send(output)
  });
}

function requestData(uri: string) {
  let query = `
    SELECT * WHERE {
        BIND(<${uri}> as ?uri) .

        OPTIONAL {
            ?uri wdt:P18 ?image .
            OPTIONAL {
                ?statement ps:P18 ?image ;
                    pq:P2096 ?imageLegend .
                FILTER(LANG(?imageLegend) = "en") .
            }
        }
        OPTIONAL {
            ?uri schema:description ?description .
            FILTER(LANG(?description) = "en") .
        }
        OPTIONAL {
            ?uri wdt:P569 ?date_of_birth .
        }
        OPTIONAL {
            ?uri wdt:P19 ?pob .
            ?pob rdfs:label ?place_of_birth .
            FILTER(LANG(?place_of_birth) = "en") .
        }
        OPTIONAL {
            ?uri wdt:P570 ?date_of_death .
        }
        OPTIONAL {
            ?uri wdt:P20 ?pod .
            ?pod rdfs:label ?place_of_death .
            FILTER(LANG(?place_of_death) = "en") .
        }
    } LIMIT 1
  `;

  return axios.get(wikidataEndpoint + "?query=" + encodeURIComponent(query));
}

export default router;
