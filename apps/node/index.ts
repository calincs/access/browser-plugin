import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

import wikidataRoutes from '@ce-node/routes/wikidata.js';
import viafRoutes from '@ce-node/routes/viaf.js';
import gettyRoutes from '@ce-node/routes/getty.js';
import iconRoutes from '@ce-node/routes/icon.js';

const PORT = process.env.PORT || 8080;
const app = express();

// Enable CORS for ALL requests (using cors middleware prior to configuring routes)
app.use(cors());
// Parse incoming Request Object if object, with nested objects, or generally any type
app.use(bodyParser.urlencoded({
  extended: true
}));
// Set static folder containing the vue app
app.use(express.static('dist'));

app.use('/wikidata', wikidataRoutes);
app.use('/viaf', viafRoutes);
app.use('/getty', gettyRoutes);
app.use('/get-icon', iconRoutes);
app.get('/healthz', (req, res) => res.send('OK'));

app.listen(PORT, () => {
  console.log(`[+] Server running on port ${PORT}`)
});
