export const translations = {
  en: {
    MainMenu: {
      currently_reading: "Currently reading",
      filters: "Filters",
      scan_whole_page: "Scan whole page",
      scan_highlighted: "Scan highlighted",
      page_name_unavailable: "Page name unavailable",
    },
    Filters: {
      back: "Back",
      ner_categories: "NER categories",
      entity_types: "Entity types",
      datasets: "Datasets",
      named_events: "Named events",
      facilities: "Facilities (buildings, airports, highways, etc.)",
      countries: "Places (countries, cities, provinces, etc.)",
      named_docs: "Named documents made into law",
      non_state: "Geographical locations (mountains, bodies of water, etc.)",
      nationalities:
        "Affiliations (nationalities, religious and political groups, etc.)",
      orgs: "Organizations",
      people: "People",
      objects: "Objects (vehicles, foods, etc.)",
      works: "Creative works",
      work: "F1 Work",
      expression: "F2 Expression",
      serial_work: "F18 Serial work",
      man_made: "E22 Human-made object",
      image: "E38 Image",
      visual: "E36 Visual item",
      digital: "D1 Digital object",
      actor: "E39 Actor",
      person: "E21 Person",
      group: "E74 Group",
      place: "E53 Place",
      adarchive: "Adarchive",
      antho_graeca: "Anthologia graeca",
      can_hist: "Historical Canadians",
      ethno: "Ethnomusicology",
      histsex: "HistSex",
      ind_aff: "Indian Affairs",
      moeml: "MOEML",
      orlando: "Orlando",
      usaskart: "USaskArt",
      y90s: "Yellow Nineties",
    },
    Settings: {
      back: "Back",
      display_settings: "Display settings",
      highlight_settings: "Highlight settings",
      english_or_french: "English or French",
      display_first: "Display first occurrence only",
      highlight_relevance: "Highlight relevance score",
    },
    Header: {
      context_explorer: "Context Explorer",
    },
  },
  fr: {
    MainMenu: {
      currently_reading: "En train de lire",
      filters: "Filtres",
      scan_whole_page: "Balayer page entière",
      scan_highlighted: "Balayer la partie en surbrillance",
      page_name_unavailable: "Nom de la page indisponible",
    },
    Filters: {
      back: "Retour",
      ner_categories: "Reconnaissance d'entités nommées",
      entity_types: "Types d'entités",
      datasets: "Jeu de données",
      named_events: "Évènements nommés",
      facilities: "Infrastructures (bâtiments, aéroports, autoroutes, etc.)",
      countries: "Lieux (pays, villes, provinces, etc.)",
      named_docs: "Documents nommés adoptés en lois",
      non_state: "Lieux géographiques (montagnes, étendues d'eau, etc.)",
      nationalities:
        "Affiliations (nationalités, religion, et groupes politiques, etc.) ",
      orgs: "Organisations",
      people: "Personnes",
      objects: "Objets (véchicles, nourriture, etc.)",
      works: "Travaux créatifs",
      work: "F1 œuvre",
      expression: "F2 Expression",
      serial_work: "F18 œuvre en série",
      man_made: "E22 Objet élaboré par l’humain",
      image: "E38 Objet conceptuel",
      visual: "E36 Entité visuelle",
      digital: "D1 Objet numérique",
      actor: "E39 Actant",
      person: "E21 Personne",
      group: "E74 Groupe",
      place: "E53 Lieu",
      adarchive: "Adarchive",
      antho_graeca: "Anthologia graeca",
      can_hist: "Canadiens historiques",
      ethno: "Ethnomusicology",
      histsex: "HistSex",
      ind_aff: "Affaires indiennes",
      moeml: "MOEML",
      orlando: "Orlando",
      usaskart: "USaskArt",
      y90s: "Yellow Nineties",
    },
    Settings: {
      back: "Retour",
      display_settings: "Paramètres d'affichage",
      highlight_settings: "Mettre en évidence les paramètres",
      english_or_french: "Anglais ou Français",
      display_first: "Afficher seulement la première occurrence",
      highlight_relevance: "Mettre en évidence l'indice de pertinence",
    },
    Header: {
      context_explorer: "Explorateur de contexte",
    },
  },
};
