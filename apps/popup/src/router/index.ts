import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import MainMenu from "../views/MainMenu.vue";
import Filters from "../views/Filters.vue";
import Settings from "../views/Settings.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "MainMenu",
    component: MainMenu,
  },
  {
    path: "/filters",
    name: "Filters",
    component: Filters,
  },
  {
    path: "/settings",
    name: "Settings",
    component: Settings,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
