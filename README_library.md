# Library params specification
Specification of the available parameters that can be passed to the CE Library
```
<script>
    window.lincs_params = {}
</script>
```

## Required

`service_worker: string,`
- The path to the service-worker.js file, relative to the domain base address
- For example: `/assets/service-worker.js`

## Optional

### Settings

All the following params should live under `settings: {}`

```
hidden: {
    settings: {
      languageSelection: {
        value: boolean
      }
    }
  },
```
- Controls the language of the UI, data, and page scan NER models
- Set as `true` for English and `false` for french
- Default: `true`

```
highlight: {
    settings: {
      displayFirstHighlight: {
        value: boolean
      }
    }
  }
```
- Sets the page scan to only highlight the first occurence of a word on the page vs all occurences of a word on the page
- Default: `false`

```
highlight: {
    settings: {
      highlightRelevanceScore: {
        value: number
      }
    }
  }
```
- Controls how close the highlights match what exists in the triplestore
- Should be a value between 1 and 20, where 1 is very broad and 20 is an exact match
- Default: `10`

### Filters

All the following params should live under `filters: {}`

```
ner: {
    filters: [
      { value: "EVENT", active: boolean },
      { value: "FAC", active: boolean },
      { value: "GPE", active: boolean },
      { value: "LAW", active: boolean },
      { value: "LOC", active: boolean },
      { value: "NORP", active: boolean },
      { value: "ORG", active: boolean },
      { value: "PERSON", active: boolean },
      { value: "PRODUCT", active: boolean },
      { value: "WORK_OF_ART", active: boolean }
    ],
  },
```
- These filters control the NER scan of the page and what entities will be highlighted
- For example, setting all active values to `false` except for PERSON, would result in the NER only tagging entities that it thinks are names of people
- Default: all active values as `true`

```
type: {
    filters: [
      { value: "http://iflastandards.info/ns/fr/frbr/frbroo/F1_Work", active: boolean },
      { value: "http://iflastandards.info/ns/fr/frbr/frbroo/F2_Expression", active: boolean },
      { value: "http://iflastandards.info/ns/fr/frbr/frbroo/F18_Serial_Work", active: boolean },
      { value: "http://www.cidoc-crm.org/cidoc-crm/E22_Man-Made_Object", active: boolean },
      { value: "http://www.cidoc-crm.org/cidoc-crm/E38_Image", active: boolean },
      { value: "http://www.cidoc-crm.org/cidoc-crm/E36_Visual_Item", active: boolean },
      { value: "http://www.ics.forth.gr/isl/CRMdig/D1_Digital_Object", active: boolean },
      { value: "http://www.cidoc-crm.org/cidoc-crm/E39_Actor", active: boolean },
      { value: "http://www.cidoc-crm.org/cidoc-crm/E21_Person", active: boolean },
      { value: "http://www.cidoc-crm.org/cidoc-crm/E74_Group", active: boolean },
      { value: "http://www.cidoc-crm.org/cidoc-crm/E53_Place", active: boolean }
    ],
  },
```
- These filters control the rdf:type of the highlighted entities
- For example, setting all active values to `false` except for E21_Person, would result in only entities of type E21_Person being highlighted
- Default: all active values as `true`

```
graph: {
    filters: [
      { value: "http://graph.lincsproject.ca/adarchive", active: boolean },
      { value: "http://graph.lincsproject.ca/anthologiaGraeca", active: boolean },
      { value: "http://graph.lincsproject.ca/hist-canada/hist-cdns", active: boolean },
      { value: "http://graph.lincsproject.ca/ethnomusicology", active: boolean },
      { value: "http://graph.lincsproject.ca/histsex", active: boolean },
      { value: "http://graph.lincsproject.ca/hist-canada/ind-affairs", active: boolean },
      { value: "http://graph.lincsproject.ca/moeml", active: boolean },
      { value: "http://graph.lincsproject.ca/orlando", active: boolean },
      { value: "http://graph.lincsproject.ca/usaskart", active: boolean },
      { value: "http://graph.lincsproject.ca/yellowNineties", active: boolean },
    ],
  },
```
- These filters control the dataset of the highlighted entities
- For example, setting all active values to `false` except for Orlando, would result in only entities that exist in the Orlando dataset being highlighted
- Default: all active values as `true`